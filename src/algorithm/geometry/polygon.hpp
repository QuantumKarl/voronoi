#if !defined( D_POLYGON_HPP )
#define D_POLYGON_HPP
#include "comparison.hpp"
#include "shapes.hpp" // box

#include <numeric>
#include <optional>
#include <sys_eigen.hpp>

namespace algorithm::geometry
{
namespace access
{
//--------------------------------
struct to_vec
{
    template < typename data_t >
    inline auto const&
        operator( )( data_t const& i_p ) const
    {
        return i_p;
    }

    template < typename data_t >
    inline auto&&
        operator( )( data_t&& i_p )
    {
        return i_p;
    }
};

//--------------------------------
struct get
{
    template < size_t index, typename vec_t >
    inline auto
        operator( )( const vec_t& i_p ) const
    {
        return i_p( index );
    }
};

//--------------------------------
struct from_vec
{
    template < typename vec_t >
    inline auto
        operator( )( vec_t const& i_p ) const
    {
        return i_p;
    }
};

//--------------------------------
struct to_vec_pair
{
    template < typename data_t >
    inline auto const&
        operator( )( data_t const& i_p ) const
    {
        return i_p.first;
    }

    template < typename data_t >
    inline auto&&
        operator( )( data_t&& i_p )
    {
        return i_p.first;
    }
};

} // end of namespace access

// TODO: add get AABB function to polygon
template < typename data_t   = vec2,           // the data type you want to store
           typename vec_t    = vec2,           // the vector type you want to convert / wrap
           typename to_vec_t = access::to_vec, // a functor to convert data_t to vec_t
           typename get_t    = access::get >      // a functor to convert vec_t to data_t
class polygon final
{
  public:
    using this_t = polygon< data_t, vec_t, to_vec_t, get_t >;

    template < typename T >
    using container_t      = aligned_vector< T >;
    using data_container_t = container_t< data_t >;
    using vec_container_t  = container_t< vec_t >;
    using data_type        = data_t;
    using vec_type         = vec_t;

    polygon( ) noexcept;
    polygon( data_container_t const& i_points ) noexcept;
    polygon( data_container_t&& i_points ) noexcept;

    void                    add_point( data_t const& i_data );
    void                    add_point( data_t&& i_data );
    auto                    remove_point( typename data_container_t::iterator i_itr );
    void                    clear_points( );
    data_container_t const& get_data_points( ) const;
    vec_container_t         get_vec_points( ) const;
    box const&              get_aabb( );

    vec_t const& get_center( );
    vec_t const& get_centroid( );
    real_t       get_area( );

    void ccw_order_points( );

    //--------------------------------
    template < typename other_data_t,
               typename other_vec_t,
               typename other_to_vec_t,
               typename other_get_t >
    auto clip( polygon< other_data_t, other_vec_t, other_to_vec_t, other_get_t > const&
                   i_subject_polygon ) const;

    //--------------------------------
    // convenience functions
    static inline vec_t   to_vec( data_t const& i_data );
    static inline vec_t&& to_vec( data_t&& i_data );

    template < size_t index >
    static inline real_t get( vec_t const& i_vec );

  private:
    static std::pair< vec_t, real_t >
        compute_centroid_and_area( data_container_t const& i_points );

    static bool inside_edge( vec_t const& i_p, vec_t const& i_e1, vec_t const& i_e2 );
    static vec_t
        intersection( vec_t const& cp1, vec_t const& cp2, vec_t const& s, vec_t const& e );

    //--------------------------------
    data_container_t       points_;
    std::optional< box >   aabb_;
    std::optional< vec_t > center_; // the average of all the points
    std::optional< vec_t >
                            centroid_; // the center of gravity (if the mass was equally distributed)
    std::optional< real_t > area_; // in 3d this would be volume
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

#include "polygon.inl"

}; // namespace algorithm::geometry

#endif
