#define WIN32_LEAN_AND_MEAN

#define CATCH_CONFIG_RUNNER
#include <catch.hpp>

#include <algorithm>
#include <iostream>
#include <map>
#include <math.h>
#include <numeric>
#include <random>
#include <sys_eigen.hpp>
#include <sys_numbers.hpp>
#include <vector>

#include "../algorithm/generate/jitter_sample.hpp"
#include "../algorithm/generate/reference.hpp"
#include "../algorithm/geometry/polygon.hpp"
#include "../algorithm/geometry/shapes.hpp"
#include "../algorithm/triangulate/delaunay.hpp"

using namespace sys;

//--------------------------------
/*
    intended for stepping through with a debugger
*/
TEST_CASE( "debug-triangle", "[delaunay]" )
{
    std::cout << "running debug triangle test" << std::endl;
    algorithm::geometry::box         bounds;
    algorithm::triangluate::delaunay triangulator;
    aligned_vector< vec2 >           random_points;

    real_t const offset = 40.0;
    real_t       width  = 1440.0;
    real_t       height = 900.0;
    bounds.min          = vec2::Zero( ) + vec2( width / 4.0, offset / 2.0 );
    bounds.max          = bounds.min + vec2( height - offset, height - offset );

    // a point off center to the left
    random_points.emplace_back( ( bounds.min + bounds.max ) / 2.0 );
    random_points[0].x( ) -= 40.0;

    // a center ish point
    random_points.emplace_back( random_points[0] );
    random_points[1].x( ) += 50.0;
    random_points[1].y( ) += 100.0;

    // a point off center to the right
    random_points.emplace_back( random_points[1] );
    random_points[2].x( ) += 50.0;

    triangulator.triangulate( random_points, bounds );

    REQUIRE( triangulator.triangles( ).size( ) == 1 );
}

//--------------------------------
template < typename triangle_container_t >
bool
    find_triangle( triangle_container_t const& i_triangles,
                   vec2 const&                 i_a,
                   vec2 const&                 i_b,
                   vec2 const&                 i_c )
{
    using tri_ptr = algorithm::triangluate::delaunay::triangle_type const*;
    std::array< std::vector< tri_ptr >, 3 > matches;

    // O(n) where n == number of triangles
    for ( auto const& tri : i_triangles )
    {
        if ( tri.a( ) == i_a || tri.b( ) == i_a || tri.c( ) == i_a )
        {
            matches[0].emplace_back( &tri );
        }

        if ( tri.a( ) == i_b || tri.b( ) == i_b || tri.c( ) == i_b )
        {
            matches[1].emplace_back( &tri );
        }

        if ( tri.a( ) == i_c || tri.b( ) == i_c || tri.c( ) == i_c )
        {
            matches[2].emplace_back( &tri );
        }
    }

    // O( k (n log n) ) where k is number of matches and n is the size of each match container
    for ( auto& match : matches )
    {
        std::sort( std::begin( match ), std::end( match ) );
    }

    // O( 2*(n+k)-1 ) where n is size of set 1, and k is size of set 2
    std::vector< tri_ptr > unions;
    std::set_intersection( std::begin( matches[0] ),
                           std::end( matches[0] ),
                           std::begin( matches[1] ),
                           std::end( matches[1] ),
                           std::back_inserter( unions ) );

    // O( 2*(n+k)-1 ) where n is size of set 1, and k is size of set 2
    std::vector< tri_ptr > unions2;
    std::set_intersection( std::begin( unions ),
                           std::end( unions ),
                           std::begin( matches[2] ),
                           std::end( matches[2] ),
                           std::back_inserter( unions2 ) );

    return unions2.size( ) == 1;
}

//--------------------------------
auto
    create_point_map( aligned_vector< vec2 > const& i_points )
{
    assert( i_points.size( ) <= 26 );
    // the points are sorted on x and then y
    // create a map of chars to points
    std::map< char,
              vec2,
              std::less< char >,
              Eigen::aligned_allocator< std::pair< const char, vec2 > > >
         point_map;
    char key = 'a';
    for ( vec2 const& p : i_points )
    {
        point_map.emplace( std::make_pair( key, p ) );
        ++key;
    }
    return point_map;
}

//--------------------------------
template < typename container_t, typename lt_func_t, typename eq_func_t >
void
    remove_duplicates( container_t& io_container, lt_func_t& lt_func, eq_func_t& eq_func )
{
    std::sort( std::begin( io_container ), std::end( io_container ), lt_func );
    io_container.erase(
        std::unique( std::begin( io_container ), std::end( io_container ), eq_func ),
        std::end( io_container ) );
}

//--------------------------------
void
    verify_result( algorithm::triangluate::delaunay& triangulator )
{
    auto const& triangles = triangulator.triangles( );
    auto const& regions   = triangulator.regions( );
    auto const& circles   = triangulator.circumcircles( );

    std::vector< algorithm::triangluate::detail::region_itr >   iterated_regions;
    std::vector< algorithm::triangluate::detail::circle_itr >   iterated_circles;
    std::vector< algorithm::triangluate::detail::triangle_itr > iterated_triangles;
    std::vector< algorithm::triangluate::detail::circle_itr >   iterated_new_circles;

    for ( auto const& t : triangles )
    {
        // the regions that the triangle's circumcirle is apart of
        auto const& triangle_regions = triangulator.iterate_triangle_regions( t );
        REQUIRE( triangle_regions.size( ) > 0 );
        for ( auto const reg_itr : triangle_regions )
        {
            iterated_regions.emplace_back( reg_itr );

            // get all points from that region
            auto const& cirlce_itrs = reg_itr->get_polygon( ).get_data_points( );
            for ( auto const circle_itr : cirlce_itrs )
            {
                // if the triangle iterator is valid
                if ( circle_itr->source_triangle_itr == std::end( triangles ) )
                {
                    REQUIRE( reg_itr->get_clipped( ) );
                    REQUIRE( circle_itr->r == 0.0 );
                }
            }
            auto const& tri_itrs = reg_itr->get_triangles( );
            for ( auto const tri_itr : tri_itrs )
            {
                iterated_triangles.emplace_back( tri_itr );
                iterated_circles.emplace_back( tri_itr->circumcircle_itr( ) );
            }
        }
    }
    REQUIRE( iterated_regions.size( ) > regions.size( ) );
    REQUIRE( iterated_circles.size( ) > triangles.size( ) );
    REQUIRE( iterated_triangles.size( ) > triangles.size( ) );
    auto lt = []( auto itr_l, auto itr_r ) { return size_t( &*itr_l ) > size_t( &*itr_r ); };
    auto eq = []( auto itr_l, auto itr_r ) { return size_t( &*itr_l ) == size_t( &*itr_r ); };
    remove_duplicates( iterated_triangles, lt, eq );
    remove_duplicates( iterated_regions, lt, eq );
    remove_duplicates( iterated_circles, lt, eq );
    remove_duplicates( iterated_new_circles, lt, eq );
    REQUIRE( iterated_regions.size( ) == regions.size( ) );
    REQUIRE( iterated_circles.size( ) == triangles.size( ) );
    REQUIRE( circles.size( ) - triangles.size( ) > 0 );
    REQUIRE( iterated_triangles.size( ) == triangles.size( ) );
}

//--------------------------------
/*
    see docs/point_reference.png for a visualization
*/
TEST_CASE( "reference-points", "[delaunay]" )
{
    std::cout << "running reference points test" << std::endl;
    algorithm::geometry::box         bounds;
    algorithm::triangluate::delaunay triangulator;
    aligned_vector< vec2 >           random_points;

    bounds.min = vec2( 1.0, 1.0 );
    bounds.max = vec2( 600.0, 600.0 );

    algorithm::generate::reference( bounds, random_points );
    auto point_map = create_point_map( random_points );

    triangulator.triangulate( random_points, bounds );
    auto const& triangles = triangulator.triangles( );
    auto const& regions   = triangulator.regions( );
    auto const& circles   = triangulator.circumcircles( );
    REQUIRE( triangles.size( ) == 11 );
    REQUIRE( regions.size( ) == 10 );
    REQUIRE( circles.size( ) == 23 ); // cirucmcirles include clipped points
    REQUIRE( find_triangle( triangles, point_map['a'], point_map['b'], point_map['d'] ) );
    REQUIRE( find_triangle( triangles, point_map['a'], point_map['d'], point_map['f'] ) );
    REQUIRE( find_triangle( triangles, point_map['a'], point_map['f'], point_map['c'] ) );
    REQUIRE( find_triangle( triangles, point_map['b'], point_map['d'], point_map['e'] ) );
    REQUIRE( find_triangle( triangles, point_map['c'], point_map['g'], point_map['f'] ) );
    REQUIRE( find_triangle( triangles, point_map['d'], point_map['e'], point_map['h'] ) );
    REQUIRE( find_triangle( triangles, point_map['d'], point_map['f'], point_map['h'] ) );
    REQUIRE( find_triangle( triangles, point_map['g'], point_map['i'], point_map['f'] ) );
    REQUIRE( find_triangle( triangles, point_map['c'], point_map['j'], point_map['g'] ) );
    REQUIRE( find_triangle( triangles, point_map['f'], point_map['i'], point_map['h'] ) );
    REQUIRE( find_triangle( triangles, point_map['g'], point_map['j'], point_map['i'] ) );

    std::vector< real_t > areas{ 38320.367422427582, 34572.769614324156, 52993.888867590962,
                                 25655.351434838652, 31675.482488610978, 31522.701301877365,
                                 28700.487682851006, 50960.709808510001, 32539.462151764579,
                                 31859.779227204672 };

    REQUIRE( areas.size( ) == regions.size( ) );
    size_t i = 0;
    for ( auto const& r : regions )
    {
        auto       polygon    = r.create_polygon( );
        auto const expected   = areas[i];
        auto const actual     = polygon.get_area( );
        bool const area_equal = algorithm::geometry::nearly_equal( expected, actual );
        REQUIRE( area_equal );
        ++i;
    }

    verify_result( triangulator );
}

//--------------------------------
/*
    see docs/jitter-sampling-1.png for a visualization
*/
TEST_CASE( "jitter-sampling-1", "[delaunay][random]" )
{
    std::cout << "running jitter sampling test" << std::endl;

    // the area of each region
    std::vector< real_t > areas{ 46346.161677208242, 40644.748692247478, 55708.284058845777,
                                 132911.77637588864, 94155.420750349673, 170402.27238119615,
                                 74187.321971927871, 120841.27322412483, 88377.568908585643,
                                 114833.80155868706, 58932.202369766746, 54953.815886021184,
                                 152058.04128757503, 91647.310857575387 };

    real_t width  = 1440.0;
    real_t height = 900.0;

    for ( size_t i = 0; i < 3; ++i )
    {
        algorithm::geometry::box         bounds;
        algorithm::triangluate::delaunay triangulator;
        aligned_vector< vec2 >           random_points;

        std::mt19937_64        generator( 5489U ); // default value from msvc's implementation
        aligned_vector< vec2 > random_points_ref;
        vec2                   random_points_grid_offset;

        bounds.min = vec2::Zero( );
        bounds.max = vec2( width, height );

        aligned_vector< vec2 > bounds_points;
        bounds_points.emplace_back( vec2( bounds.max.x( ), bounds.min.y( ) ) ); // bottom right
        bounds_points.emplace_back( bounds.max );                               // top right
        bounds_points.emplace_back( vec2( bounds.min.x( ), bounds.max.y( ) ) ); // left top
        bounds_points.emplace_back( bounds.min );                               // bottom left
        algorithm::geometry::polygon bounds_polygon( std::move( bounds_points ) );

        algorithm::generate::jitter_sample( 6,
                                            1.0 / 3.0,
                                            bounds,
                                            generator,
                                            random_points,
                                            random_points_ref,
                                            random_points_grid_offset );
        auto point_map = create_point_map( random_points );

        for ( size_t j = 0; j < 4; ++j )
        {
            triangulator.triangulate( random_points, bounds );
            auto const& triangles = triangulator.triangles( );
            auto const& regions   = triangulator.regions( );
            REQUIRE( triangles.size( ) == 16 );
            REQUIRE( regions.size( ) == 14 );
            REQUIRE(
                find_triangle( triangles, point_map['a'], point_map['b'], point_map['c'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['b'], point_map['d'], point_map['c'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['a'], point_map['c'], point_map['e'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['c'], point_map['d'], point_map['e'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['d'], point_map['e'], point_map['f'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['e'], point_map['f'], point_map['g'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['f'], point_map['g'], point_map['h'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['g'], point_map['h'], point_map['i'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['f'], point_map['h'], point_map['k'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['f'], point_map['j'], point_map['k'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['f'], point_map['j'], point_map['k'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['j'], point_map['k'], point_map['l'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['h'], point_map['i'], point_map['m'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['k'], point_map['l'], point_map['n'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['k'], point_map['m'], point_map['n'] ) );
            REQUIRE(
                find_triangle( triangles, point_map['j'], point_map['l'], point_map['n'] ) );

            REQUIRE( areas.size( ) == regions.size( ) );
            real_t total_area = 0.0;
            size_t k          = 0;
            for ( auto const& r : regions )
            {
                auto         polygon  = r.create_polygon( );
                real_t const expected = areas[k];
                real_t const actual   = polygon.get_area( );
                // TODO: check why reduced precision was required
                bool const area_equal =
                    algorithm::geometry::nearly_equal( float( expected ), float( actual ) );
                REQUIRE( area_equal );

                total_area += actual;
                ++k;
            }

            // the sum of the region areas is the same as the area of the bounds (the box which all triangle_regions are contained in)
            real_t const total_actual = total_area;
            real_t const total_bounds = bounds_polygon.get_area( );
            bool const   area_equal =
                algorithm::geometry::nearly_equal( total_actual, total_bounds );
            REQUIRE( area_equal );

            verify_result( triangulator );
        }

        // scale the area
        width *= 2.0;
        height *= 2.0;
        for ( auto& area : areas )
        {
            area *= 4.0;
        }
    }
}

//--------------------------------
void
    area_test( aligned_vector< vec2 >&& points, real_t expected )
{
    algorithm::geometry::polygon poly( std::move( points ) );
    auto const                   actual = std::abs(
        poly.get_area( ) ); // depending on the order of the points, the output's sign changes
    bool const area_equal =
        algorithm::geometry::nearly_equal( float( expected ), float( actual ) );
    REQUIRE( area_equal );
}

//--------------------------------
real_t
    regular_polygon_area( size_t n )
{
    return ( real_t( n ) / 2.0 ) * std::sin( ( 2.0 * pi ) / real_t( n ) );
}

//--------------------------------
aligned_vector< vec2 >
    regular_polygon( size_t count )
{
    real_t const           r = 1.0;
    aligned_vector< vec2 > points;

    for ( size_t i = 0; i < count; ++i )
    {
        points.emplace_back( vec2( std::cos( ( 2.0 * pi ) / real_t( count ) * i ) * r,
                                   std::sin( ( 2.0 * pi ) / real_t( count ) * i ) * r ) );
    }
    return points;
}

//--------------------------------
TEST_CASE( "polygon_area_test", "[geometry]" )
{
    std::cout << "running polygon area test" << std::endl;
    // unit square
    area_test( { vec2( -0.5, -0.5 ), vec2( -0.5, 0.5 ), vec2( 0.5, 0.5 ), vec2( 0.5, -0.5 ) },
               1.0 );

    // right angle triangle (half unit square)
    area_test( { vec2( -0.5, -0.5 ), vec2( -0.5, 0.5 ), vec2( 0.5, 0.5 ) }, 0.5 );

    // regular polygons
    for ( size_t i = 3; i < 256; ++i )
    {
        area_test( regular_polygon( i ), regular_polygon_area( i ) );
    }
}

//--------------------------------
void
    box_circile_collision_test( real_t denominator, real_t circle_rad )
{
    algorithm::geometry::circle c( vec2::Zero( ), circle_rad );
    for ( size_t i = 1; i < 10; ++i )
    {
        real_t                   offset = real_t( i ) / denominator;
        vec2                     p1( offset, offset );
        vec2                     p2 = p1 + vec2::Ones( );
        algorithm::geometry::box b( p1, p2 );
        bool                     bo = algorithm::geometry::box_circle_collision( b, c );
        if ( offset <= std::sqrt( circle_rad / 2.0 ) )
        {
            REQUIRE( bo == true );
        }
        else
        {
            REQUIRE( bo == false );
        }
    }
}

//--------------------------------
TEST_CASE( "box_circle_collision", "[geometry]" )
{
    std::cout << "running box circle collision test" << std::endl;
    box_circile_collision_test( 10, 1.0 );
    box_circile_collision_test( -10, 1.0 );
    box_circile_collision_test( 10, 2.0 );
    box_circile_collision_test( -10, 2.0 );
}

//--------------------------------
template < size_t size >
void
    box_ray_test( aligned_vector< algorithm::geometry::ray > const& rays,
                  algorithm::geometry::box const&                   box,
                  std::array< bool, size > const&                   results,
                  std::array< real_t, size > const&                 mins,
                  std::array< real_t, size > const&                 maxs )
{
    using namespace algorithm::geometry;
    for ( size_t i = 0; i < size; ++i )
    {
        ray const&    current_ray        = rays[i];
        bool const&   expected_collision = results[i];
        real_t const& expectd_min        = mins[i];
        real_t const& expectd_max        = maxs[i];

        real_t min = std::numeric_limits< real_t >::max( );
        real_t max = std::numeric_limits< real_t >::min( );

        bool const collide = box_ray_collision( box, current_ray, min, max );
        REQUIRE( collide == expected_collision );
        if ( collide == true )
        {
            REQUIRE( nearly_equal( min, expectd_min ) );
            REQUIRE( nearly_equal( max, expectd_max ) );
        }
    }
}

//--------------------------------
TEST_CASE( "box_ray_collision", "[geometry]" )
{
    std::cout << "running box ray collision test" << std::endl;
    using namespace algorithm::geometry;
    constexpr real_t eps   = std::numeric_limits< real_t >::epsilon( );
    constexpr size_t count = 8;

    box const    b( vec2( -1.0, -1.0 ), vec2( 1.0, 1.0 ) );
    real_t const offset = 1.0;
    real_t const dist   = std::sqrt( offset + offset );
    real_t const sqrt   = ( b.max - b.min ).norm( );

    aligned_vector< ray > rays{ // ray pointing -y towards origin into box at origin:
                                ray( vec2( eps, 2.0 ), vec2( 0.0, -1.0 ).normalized( ) ),
                                // ray pointing -x and -y towards origin into box at origin:
                                ray( vec2( 2.0, 2.0 ), vec2( -0.5, -0.5 ).normalized( ) ),
                                // ray pointing on -x into box at origin:
                                ray( vec2( 2.0, eps ), vec2( -1.0, eps ).normalized( ) ),
                                // ray pointing -x and +y towards origin into box at origin
                                ray( vec2( 2.0, -2.0 ), vec2( -0.5, 0.5 ).normalized( ) ),
                                // ray pointing on +y into box at origin
                                ray( vec2( eps, -2.0 ), vec2( 0.0, 1.0 ).normalized( ) ),
                                // ray pointing +x and +y towards origin into box at origin
                                ray( vec2( -2.0, -2.0 ), vec2( 0.5, 0.5 ).normalized( ) ),
                                // ray pointing on +x into box at origin
                                ray( vec2( -2.0, eps ), vec2( 1.0, eps ).normalized( ) ),
                                // ray pointing +x and -y towards origin into box at origin
                                ray( vec2( -2.0, 2.0 ), vec2( 0.5, -0.5 ).normalized( ) )
    };
    REQUIRE( rays.size( ) == count );
    std::array< bool, count >   results{ true, true, true, true, true, true, true, true };
    std::array< real_t, count > mins{ 1.0, dist, 1.0, dist, 1.0, dist, 1.0, dist };
    std::array< real_t, count > max{ 3.0, dist + sqrt, 3.0, dist + sqrt,
                                     3.0, dist + sqrt, 3.0, dist + sqrt };

    // 8 rays pointing inwards to the cube, from outside the cube
    box_ray_test( rays, b, results, mins, max );

    // flip the rays
    for ( auto& r : rays )
    {
        r.d( ) *= -1.0;
        r.update_inv( );
    }

    // flip the results
    for ( auto& r : results )
    {
        r = !r;
    }
    // mins and maxs wont be checked because they dont collide

    // 8 rays pointing outwards from the cube, from outside the cube
    box_ray_test( rays, b, results, mins, max );

    // move ray origins
    for ( auto& r : rays )
    {
        r.o( ) = vec2::Zero( );
    }

    // flip the results (from false to true)
    for ( auto& r : results )
    {
        r = !r;
    }

    // maxs now become that past mins
    // (the distance the rays originated from are the same as origin to the edge of the cube)
    max = mins;

    // flip the sign of mins
    for ( auto& m : mins )
    {
        m *= -1.0;
    }

    // 8 rays pointing outwards from the cube, from inside the cube
    box_ray_test( rays, b, results, mins, max );
}

//--------------------------------
int
    main( int argc, char* argv[] )
{
    int const result = Catch::Session( ).run( argc, argv );

    return result;
}
