#if !defined( D_CIRCUMCIRCLE_HPP )
#define D_CIRCUMCIRCLE_HPP

#include "../../geometry/shapes.hpp"
#include "triangle.hpp"

#include <list>
#include <sys_trie.hpp>
#include <vector>

namespace algorithm::triangluate::detail
{
using namespace sys;

//--------------------------------
class circumcircle : public geometry::circle
{
  public:
    template < typename... args_t >
    circumcircle( triangle_itr i_itr, args_t&&... i_args ) noexcept
        : circle( std::forward< args_t >( i_args )... )
        , source_triangle_itr( i_itr )
    {
    }

    triangle_itr source_triangle_itr;
};

} // namespace algorithm::triangluate::detail

#endif
