#if !defined( D_REGION_HPP )
#define D_REGION_HPP

#include "../../geometry/polygon.hpp"
#include "circumcircle.hpp"
#include "hashes.hpp"
#include "triangle.hpp"

#include <map>
#include <optional>
#include <sys_eigen.hpp>
#include <sys_trie.hpp>

namespace algorithm::triangluate::detail
{
using namespace sys;

//----------------------------------------------------------------
// accessor functors for polygon class

//--------------------------------
struct to_vec
{
    inline vec2 const&
        operator( )( circle_itr const& i_p ) const
    {
        return i_p->p;
    }
};

//--------------------------------
struct get
{
    template < size_t index >
    inline auto
        operator( )( const vec2& i_p ) const
    {
        return i_p( index );
    }
};

struct vec2_equal
{
    bool
        operator( )( vec2 const& l, vec2 const& r ) const
    {
        real_t const& x1 = l.x( );
        real_t const& y1 = l.y( );
        real_t const& x2 = r.x( );
        real_t const& y2 = r.y( );
        if ( geometry::nearly_equal( x1, x2 ) == false )
        {
            return x1 < x2;
        }
        else
        {
            return y1 < y2;
        }
    };
};

// TODO: change with flat map
using new_point_map_t = std::map< vec2, circle_itr, vec2_equal >;

using circle_itr_polygon = geometry::polygon< circle_itr, vec2, detail::to_vec, detail::get >;

// TODO: consider, the circle itrs which dont have a valid triangle itr are points which are
// added from the polygon clipping, the user may want to know this information
//--------------------------------
/* region
    A voronoi region, it is output from the regionalate function.
*/
class region
{
  public:
    region( vec2 const&           i_seed,
            bool                  i_clipped,
            circle_itr_polygon&&  i_poly,
            triangle_itr_cont_t&& i_triangles ) noexcept;

    triangle_itr_cont_t const& get_triangles( ) const;
    vec2 const&                get_seed( ) const;
    bool                       get_clipped( ) const;
    circle_itr_polygon const&  get_polygon( ) const;
    geometry::polygon<>        create_polygon( ) const;

  private:
    circle_itr_polygon  polygon_;
    triangle_itr_cont_t triangles_;
    vec2                seed_;
    bool                clipped_;
};

//--------------------------------
// region trie containers
using region_cont_t      = std::list< region >;
using region_itr         = region_cont_t::iterator;
using region_edge_cont_t = std::list< std::array< region_itr, 2 > >;

using region_point_map_value = region_edge_cont_t::iterator;
using region_point_map_t =
    htrie_map< char, region_point_map_value, xxhash_data, unsigned char >;

//--------------------------------
template < typename itr_type >
class iterator_wrapper final
{
  public:
    iterator_wrapper( itr_type i_begin, itr_type i_end ) noexcept
        : begin_itr( i_begin )
        , end_itr( i_end )
    {
    }

    decltype( auto )
        begin( ) const
    {
        return begin_itr;
    }

    decltype( auto )
        end( ) const
    {
        return end_itr;
    }

  private:
    itr_type begin_itr;
    itr_type end_itr;
};

using regions_itr_set_t = std::unordered_set< region_itr, deref_addressof_hash >;

} // namespace algorithm::triangluate::detail

#endif
