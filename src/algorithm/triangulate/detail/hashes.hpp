#if !defined( D_HASHES_HPP )
#define D_HASHES_HPP

#include <sys_eigen.hpp>
#include <sys_hash.hpp>

namespace algorithm::triangluate::detail
{
using namespace sys;

//--------------------------------
struct xxhash_data
{
    std::size_t
        operator( )( char const* key, std::size_t key_size ) const
    {
        assert( key_size <= 64 ); // NOTE: this hash is only used for value_edges
        return XXH64( key, key_size, 1077 );
    }
};

struct deref_addressof_hash
{
    template < typename itr_t >
    size_t
        operator( )( itr_t const& itr ) const
    {
        return std::hash< size_t >( )( size_t( &*itr ) );
    }
};

struct addressof_hash
{
    template < typename ref_t >
    size_t
        operator( )( ref_t const& ref ) const
    {
        return std::hash< size_t >( )( size_t( &ref ) );
    }
};

struct vec2_hash
{
    size_t
        operator( )( vec2 const& vec ) const
    {
        real_t const x = vec.x( );
        real_t const y = vec.y( );
        return std::hash< real_t >( )( x ) ^ std::hash< real_t >( )( y );
    }
};

} // namespace algorithm::triangluate::detail

#endif
