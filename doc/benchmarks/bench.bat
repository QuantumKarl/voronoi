@echo off
echo "Started countdown"
timeout /t 10 >nul
echo "Stared benchmark" & time /t
call "../../ide/msvs2019/bin/x64/voronoi-benchmarkRelease.exe" -r xml --benchmark-samples 100  > results.xml 2>&1
echo "Finished benchmark" & time /t
call activate root
python "plot_results.py" results.xml