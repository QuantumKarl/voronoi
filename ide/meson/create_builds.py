# -*- coding: utf-8 -*-

#------------------------ create builds ------------------------
# This python script is called by the user on the commandline.
# Depending on the OS, it generates the ninja build files via
# calling meson with the correct parameters.

import os
import sys
import subprocess
import platform
# TODO: change
def runMeson(set,has_debug,has_opt,folder):
    args = [set+'meson',
			'-Dbuildtype=plain',
            '-Dwarning_level=0',
            '-Db_pch=false',
            '-Dcpp_rtti=false',
            '-Dhas_debug={0}'.format(has_debug),
			'-Dhas_opt={0}'.format(has_opt),
            '"{0}"'.format(folder)]

    cmd = args[0]
    for i in range(1,len(args) ):
        cmd = cmd + ' ' + args[i]
    print('meson command: {0}'.format(cmd))
    print(subprocess.check_output(cmd, shell=True).decode().replace('\r\n','\n') )

def main(argv):
    if platform.system() == 'Windows':
        runMeson('','true','false', 'msvc_debug')
        runMeson('','false','true', 'msvc_release')
        runMeson('set CC=clang-cl CXX=clang-cl && ', 'false','true', 'clangcl_release')
    elif platform.system() == 'Linux':
        runMeson('CC=clang CXX=clang++ LD=ld.lld ','true','false', 'clang_debug')
        runMeson('CC=clang CXX=clang++ LD=ld.lld ','false','true', 'clang_release')
    else:
        print("Error: unsupported OS")

    return 0

if __name__ == '__main__':
    main(sys.argv)