# -*- coding: utf-8 -*-
import sys
from matplotlib import pyplot as plt
from matplotlib import style
import xml.etree.ElementTree as ET

def simplePlot(x,y, title):
    style.use('ggplot')
    plt.bar(x, y, align='center')
    plt.title(title)
    #plt.yscale('log', basey=2)
    plt.ylabel('Time in seconds')
    plt.xlabel('Input size')
    
    plt.show()


def extractInfo(filePath,testCaseName,totalTime=True):
    tree = ET.parse(filePath)
    root = tree.getroot()
    
    names = []
    sizes = []
    times = []
    for result in root.findall("./Group/TestCase[@name='"+testCaseName+"']/BenchmarkResults"):
        names.append(result.attrib["name"])
        if totalTime:
            times.append(int(result.attrib["estimatedDuration"])/1000000/1000)
        else:
            deviation = result.find("standardDeviation")
            times.append(float(deviation.attrib["value"])/1000000)
    
    for name in names:
        s1 = name.split(",")
        assert( len(s1) == 2 )
        s2 = s1[1].split(":")
        assert( len(s2) == 2 )
        sizeAsStr = s2[1].replace('}','')
        sizes.append(sizeAsStr)
    
    return (sizes,times)

def plotTest(fileSource, testName, testTtitle):
    (names,times) =  extractInfo(fileSource, testName)
    simplePlot(names,times,testTtitle)

def main(argv):
    if len(argv) == 2:
        file = argv[1]
        plotTest(file,"jitter-triangulate","Triangulation of jitter sampling")
        plotTest(file,"hex-triangulate","Triangulation of hex grid center points")
    else:
        plotTest("results_0/results.xml","jitter-triangulate","Triangulation of jitter sampling")
        
    return 0

if __name__ == '__main__':
    main(sys.argv)