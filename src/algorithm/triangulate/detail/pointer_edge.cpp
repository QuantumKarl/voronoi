#include "pointer_edge.hpp"

namespace algorithm::triangluate::detail
{
//--------------------------------
pointer_edge::pointer_edge( vec2 const& i_a, vec2 const& i_b ) noexcept
    : points_{ &i_a, &i_b }
{
}

//--------------------------------
vec2 const&
    pointer_edge::a( ) const
{
    return *points_[0];
}

//--------------------------------
vec2 const&
    pointer_edge::b( ) const
{
    return *points_[1];
}

} // namespace algorithm::triangluate::detail
