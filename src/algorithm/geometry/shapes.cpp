#include "shapes.hpp"

namespace algorithm::geometry
{
using namespace sys;

//--------------------------------
box::box( ) noexcept
    : min( )
    , max( )
{
}

//--------------------------------
box::box( vec2 const& i_min, vec2 const& i_max ) noexcept
    : min( i_min )
    , max( i_max )
{
}

//--------------------------------
box::box( vec2&& i_min, vec2&& i_max ) noexcept
    : min( std::forward< vec2 >( i_min ) )
    , max( std::forward< vec2 >( i_max ) )
{
}

//--------------------------------
circle::circle( ) noexcept
    : p( )
    , r( )
{
}

//--------------------------------
circle::circle( vec2 const& i_v, real_t const i_radius ) noexcept
    : p( i_v )
    , r( i_radius )
{
}

//--------------------------------
circle::circle( vec2&& i_v, real_t const i_radius ) noexcept
    : p( std::forward< vec2 >( i_v ) )
    , r( i_radius )
{
}

//--------------------------------
triangle::triangle( ) noexcept
    : points{ }
{
    points.resize( 3 );
}

//--------------------------------
triangle::triangle( vec2 const& i_a, vec2 const& i_b, vec2 const& i_c ) noexcept
    : points{ i_a, i_b, i_c }
{
}

//--------------------------------
triangle::triangle( vec2&& i_a, vec2&& i_b, vec2&& i_c ) noexcept
    : points{ std::forward< vec2 >( i_a ),
              std::forward< vec2 >( i_b ),
              std::forward< vec2 >( i_c ) }
{
}

//--------------------------------
bool
    circle::point_collision( vec2 const& i_point ) const
{
    vec2 const   pp  = ( p - i_point );
    real_t const mag = pp.squaredNorm( );
    return mag <= ( r * r );
}

//--------------------------------
vec2&
    triangle::a( )
{
    return points[0];
}

//--------------------------------
vec2&
    triangle::b( )
{
    return points[1];
}

//--------------------------------
vec2&
    triangle::c( )
{
    return points[2];
}

//--------------------------------
vec2 const&
    triangle::a( ) const
{
    return points[0];
}

//--------------------------------
vec2 const&
    triangle::b( ) const
{
    return points[1];
}

//--------------------------------
vec2 const&
    triangle::c( ) const
{
    return points[2];
}

//--------------------------------
// https://www.ics.uci.edu/~eppstein/junkyard/circumcenter.html
// NOTE: 2D only
circle
    triangle::create_circumcircle( ) const
{
    // use coordinates relative to point `a' of the triangle
    vec2 const ba = b( ) - a( );
    vec2 const ca = c( ) - a( );

    // squares of lengths of the edges incident to `a'
    real_t const ba_mag = ba.squaredNorm( );
    real_t const ca_mag = ca.squaredNorm( );
    vec2 const   caS    = ca * ba_mag;
    vec2 const   baS    = ba * ca_mag;

    // calculate the denominator of the formula
    real_t const denom = 0.5 / ( ( ba.x( ) * ca.y( ) ) - ( ba.y( ) * ca.x( ) ) ); // 2D cross

    real_t const cX = ( caS.y( ) - baS.y( ) ) * denom;
    real_t const cY = ( baS.x( ) - caS.x( ) ) * denom;

    // center relative to a
    vec2 const center = vec2( cX, cY );

    return circle( a( ) + center, center.norm( ) );
}
#if 0
    // small test
    vec2              a( 0.0, 0.0 );
    vec2              b( 0.0, 1.0 );
    vec2              c( 1.0, 0.0 );
    std::list< edge > edges;
    vec2 ab = emplace_back( edges, a, b );
    vec2 bc = emplace_back( edges, b, c );
    vec2 ca = emplace_back( edges, c, a );
    triangle          tri( ab, bc, ca );
    auto circle = triangle_circumcircle( tri );
    unused( circle );
#endif

//--------------------------------
ray::ray( ) noexcept {}

//--------------------------------
ray::ray( vec2 const& i_o, vec2 const& i_dir ) noexcept
    : o_( i_o )
    , d_( i_dir )
    , inv_( d_.cwiseInverse( ) )
{
}

//--------------------------------
ray::ray( vec2&& i_o, vec2&& i_dir ) noexcept
    : o_( std::forward< vec2 >( i_o ) )
    , d_( std::forward< vec2 >( i_dir ) )
    , inv_( d_.cwiseInverse( ) )
{
}

//--------------------------------
vec2&
    ray::o( )
{
    return o_;
}

//--------------------------------
vec2&
    ray::d( )
{
    return d_;
}

//--------------------------------
vec2 const&
    ray::o( ) const
{
    return o_;
}

//--------------------------------
vec2 const&
    ray::d( ) const
{
    return d_;
}

//--------------------------------
vec2 const&
    ray::inv( ) const
{
    return inv_;
}

//--------------------------------
void
    ray::update_inv( )
{
    inv_ = d_.cwiseInverse( );
}

//--------------------------------
/* box vs ray collision detect
https://tavianator.com/fast-branchless-raybounding-box-intersections/
This collision function is fragile.
* If the ray originates inside the box: tmin is negative and tmax is the distance to the point.
* If the ray's origin in on an edge / corner the box it does not work (due to NaNs)

bool intersection(box b, ray r) {
    double tx1 = (b.min.x - r.x0.x)*r.n_inv.x;
    double tx2 = (b.max.x - r.x0.x)*r.n_inv.x;

    double ty1 = (b.min.y - r.x0.y)*r.n_inv.y;
    double ty2 = (b.max.y - r.x0.y)*r.n_inv.y;

    double txmin = min(tx1, tx2);
    double txmax = max(tx1, tx2);

    double tymin = min(ty1, ty2);
    double tymax = max(ty1, ty2);

    double tmin = max(txmin, tymin);
    double tmax = min(txmax, tymax);

    return tmax >= 0.0 && tmax >= tmin;
}
*/
bool
    box_ray_collision( box b, ray const& r, real_t& o_tmin, real_t& o_tmax )
{
    vec2 t1 = ( b.min - r.o( ) ).cwiseProduct( r.inv( ) );
    vec2 t2 = ( b.max - r.o( ) ).cwiseProduct( r.inv( ) );

    real_t tmin = t1.cwiseMin( t2 ).maxCoeff( );
    real_t tmax = t1.cwiseMax( t2 ).minCoeff( );

    o_tmin = tmin;
    o_tmax = tmax;

    return tmax >= 0.0 && tmin <= tmax;
}

//--------------------------------
// https://yal.cc/rectangle-sphere2d-intersection-test/
bool
    box_circle_collision( box const& i_box, circle const& i_s )
{
    vec2 const rect = i_box.min;
    vec2 const size = i_box.max - i_box.min;

    vec2 delta = i_s.p - rect.cwiseMax( i_s.p.cwiseMin( rect + size ) );
    return delta.squaredNorm( ) < ( i_s.r * i_s.r );
}

//--------------------------------
bool
    box_point_collision( box const& i_box, vec2 const& i_point )
{
    return i_point.x( ) >= i_box.min.x( ) && i_point.x( ) <= i_box.max.x( ) &&
           i_point.y( ) >= i_box.min.y( ) && i_point.y( ) <= i_box.max.y( );
}

//--------------------------------
ray
    points_to_ray( vec2 const& a, vec2 const& b )
{
    return ray( a, ( b - a ).normalized( ) );
}

} // namespace algorithm::geometry
