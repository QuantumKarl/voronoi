Log of Meson test suite run on 2020-07-07T21:54:08.980126

Inherited environment: HOSTNAME='1ce1b41d4064' SHLVL='1' HOME='/root' OLDPWD='/repos/voronoi/ide/meson' TERM='xterm' PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' PWD='/repos/voronoi/ide/meson/clang_release' 

1/1 unit                                    OK       0.72 s 

--- command ---
valgrind --tool=memcheck /repos/voronoi/ide/meson/clang_release/subprojects/test/voronoi-tests exclude:[random]
--- stdout ---
Filters: exclude:[random]
running debug triangle test
running reference points test
running polygon area test
running box circle collision test
running box ray collision test
===============================================================================
All tests passed (490 assertions in 5 test cases)

--- stderr ---
==305== Memcheck, a memory error detector
==305== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==305== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==305== Command: /repos/voronoi/ide/meson/clang_release/subprojects/test/voronoi-tests exclude:[random]
==305== 
==305== 
==305== HEAP SUMMARY:
==305==     in use at exit: 74,087 bytes in 10 blocks
==305==   total heap usage: 6,226 allocs, 6,216 frees, 1,945,332 bytes allocated
==305== 
==305== LEAK SUMMARY:
==305==    definitely lost: 0 bytes in 0 blocks
==305==    indirectly lost: 0 bytes in 0 blocks
==305==      possibly lost: 0 bytes in 0 blocks
==305==    still reachable: 74,087 bytes in 10 blocks
==305==         suppressed: 0 bytes in 0 blocks
==305== Rerun with --leak-check=full to see details of leaked memory
==305== 
==305== For lists of detected and suppressed errors, rerun with: -s
==305== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
-------


Ok:                    1
Expected Fail:         0
Fail:                  0
Unexpected Pass:       0
Skipped:               0
Timeout:               0
