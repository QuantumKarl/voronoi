import os
import sys
import subprocess

fileTypes = [".cpp", ".c", ".hpp", ".h", ".inl"]
folderSkipList = ["imgui","entry"]

def getSource(path):
    filePaths = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if( any( map( lambda ext : file.endswith(ext), fileTypes ) ) ):
                    filePaths.append( os.path.join(root, file) )
    return filePaths

def main(argv):
    os.chdir(os.path.join("..",".."))
    sourceFiles = getSource("src")
    for file in sourceFiles:
        fileName = os.path.basename(file)
        folderName = os.path.basename(os.path.dirname(file))
        if folderName in folderSkipList:
            print("skipping " + fileName + " " + folderName)
            continue
        print("formatting: " + fileName)
        result = subprocess.run(["clang-format","-style=file", "-i", file], timeout=30, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        stdout = result.stdout.decode()
        stderr = result.stderr.decode()
        if len(stdout) > 0:
            print(stdout)
        if len(stderr) > 0:
            print(stderr)

if __name__ == '__main__':
    main(sys.argv)