#if !defined( D_FIXTURE_HPP )
#define D_FIXTURE_HPP

#include <algorithm>
#include <numeric>
#include <random>
#include <sys_eigen.hpp>
#include <vector>

#include "../algorithm/generate/hex_grid.hpp"
#include "../algorithm/generate/jitter_sample.hpp"
#include "../algorithm/geometry/shapes.hpp"
#include "../algorithm/triangulate/delaunay.hpp"

using namespace sys;

class fixture final
{
  public:
    //--------------------------------
    template < typename generator_func_t >
    fixture( generator_func_t i_generator_func )
        : bounds( )
        , triangulator( )
        , random_points( )
        , generator_func( i_generator_func )
        , generator( 5489U )
        , random_points_ref( )
        , random_points_grid_offset( )
        , count_( )
    {
    }

    //--------------------------------
    void
        init( size_t const i_power )
    {
        auto const size         = ( size_t( 1 ) << i_power );
        auto const subdivisions = static_cast< size_t >( std::sqrt( size ) + 0.5 );
        bounds.min              = vec2::Zero( );
        bounds.max              = vec2( size, size );

        generator_func( subdivisions,
                        1.0,
                        bounds,
                        generator,
                        random_points,
                        random_points_ref,
                        random_points_grid_offset );

        count_ = random_points.size( );
    }

    //--------------------------------
    size_t
        get_count( ) const
    {
        return count_;
    }

    //--------------------------------
    void
        triangulate( )
    {
        triangulator.triangulate( random_points, bounds );
    }

  private:
    algorithm::geometry::box         bounds;
    algorithm::triangluate::delaunay triangulator;
    aligned_vector< vec2 >           random_points;
    std::function< void( size_t const                    i_subdivision,
                         real_t const                    i_pass_rate,
                         algorithm::geometry::box const& i_area,
                         std::mt19937_64&                io_generator,
                         aligned_vector< vec2 >&         io_random_points,
                         aligned_vector< vec2 >&         io_ref_points,
                         vec2&                           io_halfwidths ) >
        generator_func;

    std::mt19937_64        generator; // recreated per test (but the area will be different)
    aligned_vector< vec2 > random_points_ref;
    std::vector< algorithm::triangluate::delaunay::region_type > regions_;
    vec2                                                         random_points_grid_offset;
    size_t                                                       count_;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

#endif
