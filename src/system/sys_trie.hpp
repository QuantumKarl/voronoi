#if !defined( D_TRIE_HPP )
#define D_TRIE_HPP

DISABLE_WARNINGS

#include <htrie_map.h>
#include <htrie_set.h>

ENABLE_WARNINGS

#endif
