#define D_CLASS_TEMPLATE \
    template < typename data_t, typename vec_t, typename to_vec_t, typename get_t >

#define D_TEMPLATE_ARGS data_t, vec_t, to_vec_t, get_t

//--------------------------------
D_CLASS_TEMPLATE
polygon< D_TEMPLATE_ARGS >::polygon( ) noexcept
    : points_( )
{
}

//--------------------------------
D_CLASS_TEMPLATE
polygon< D_TEMPLATE_ARGS >::polygon( data_container_t const& i_points ) noexcept
    : points_( i_points )
{
}

//--------------------------------
D_CLASS_TEMPLATE
polygon< D_TEMPLATE_ARGS >::polygon( data_container_t&& i_points ) noexcept
    : points_( std::forward< data_container_t >( i_points ) )
{
}

//--------------------------------
D_CLASS_TEMPLATE
void
    polygon< D_TEMPLATE_ARGS >::add_point( data_t const& i_data )
{
    points_.emplace_back( i_data );
}

//--------------------------------
D_CLASS_TEMPLATE
auto
    polygon< D_TEMPLATE_ARGS >::remove_point( typename data_container_t::iterator i_itr )
{
    return points_.erase( i_itr );
}

//--------------------------------
D_CLASS_TEMPLATE
void
    polygon< D_TEMPLATE_ARGS >::add_point( data_t&& i_data )
{
    points_.emplace_back( std::forward< data_t >( i_data ) );
}

//--------------------------------
D_CLASS_TEMPLATE
void
    polygon< D_TEMPLATE_ARGS >::clear_points( )
{
    points_.clear( );
}

//--------------------------------
D_CLASS_TEMPLATE
typename polygon< D_TEMPLATE_ARGS >::data_container_t const&
    polygon< D_TEMPLATE_ARGS >::get_data_points( ) const
{
    return points_;
}

//--------------------------------
D_CLASS_TEMPLATE
typename polygon< D_TEMPLATE_ARGS >::vec_container_t
    polygon< D_TEMPLATE_ARGS >::get_vec_points( ) const
{
    vec_container_t vec_points;
    vec_points.reserve( points_.size( ) );
    for ( auto& data : points_ )
    {
        vec_points.emplace_back( to_vec( data ) );
    }
    return vec_points;
}

//--------------------------------
D_CLASS_TEMPLATE
box const&
    polygon< D_TEMPLATE_ARGS >::get_aabb( )
{
    if ( aabb_.has_value( ) )
    {
        return aabb_.value( );
    }

    constexpr real_t const max_value = std::numeric_limits< real_t >::max( );
    constexpr real_t const min_value = std::numeric_limits< real_t >::min( );
    real_t                 min_x( max_value );
    real_t                 min_y( max_value );
    real_t                 max_x( min_value );
    real_t                 max_y( min_value );

    for ( vec2 const& p : points_ )
    {
        min_x = p.x( ) < min_x ? p.x( ) : min_x;
        min_y = p.y( ) < min_y ? p.y( ) : min_y;
        max_x = p.x( ) > max_x ? p.x( ) : max_x;
        max_y = p.y( ) > max_y ? p.y( ) : max_y;
    }
    aabb_.emplace( vec2( min_x, min_y ), vec2( max_x, max_y ) );
    return aabb_.value( );
}

//--------------------------------
D_CLASS_TEMPLATE
vec_t const&
    polygon< D_TEMPLATE_ARGS >::get_center( )
{
    assert( points_.size( ) > 0 );
    if ( center_.has_value( ) )
    {
        return center_.value( );
    }
    vec_t acc( 0.0, 0.0 );
    for ( auto& d : points_ )
    {
        acc += to_vec( d );
    }
    vec_t temp = acc / static_cast< real_t >( points_.size( ) );

    center_.emplace( temp );

    return center_.value( );
}

//--------------------------------
D_CLASS_TEMPLATE
vec_t const&
    polygon< D_TEMPLATE_ARGS >::get_centroid( )
{
    assert( points_.size( ) > 0 );
    if ( centroid_.has_value( ) )
    {
        return centroid_.value( );
    }
    auto results = compute_centroid_and_area( points_ );
    centroid_.emplace( results.first );
    area_.emplace( results.second );

    return centroid_.value( );
}

//--------------------------------
D_CLASS_TEMPLATE
real_t
    polygon< D_TEMPLATE_ARGS >::get_area( )
{
    assert( points_.size( ) > 0 );
    if ( area_.has_value( ) )
    {
        return area_.value( );
    }
    auto results = compute_centroid_and_area( points_ );
    centroid_.emplace( results.first );
    area_.emplace( results.second );

    return area_.value( );
}

//--------------------------------
D_CLASS_TEMPLATE
void
    polygon< D_TEMPLATE_ARGS >::ccw_order_points( )
{
    vec_t       acc( 0.0, 0.0 );
    vec_t const center = get_center( );

    auto const compare_func = [&]( data_t const& l, data_t const& r ) {
        return algorithm::geometry::less( center, to_vec( r ), to_vec( l ) );
    };

    std::sort( std::begin( points_ ), std::end( points_ ), compare_func );
}

//--------------------------------
D_CLASS_TEMPLATE
std::pair< vec_t, real_t >
    polygon< D_TEMPLATE_ARGS >::compute_centroid_and_area( data_container_t const& i_points )
{
    real_t signed_area( 0.0 );
    real_t acc( 0.0 );
    vec_t  centroid( 0.0, 0.0 );

    for ( size_t i = 0, c = i_points.size( ); i < c; ++i )
    {
        vec_t const& p1  = to_vec( i_points[i] );
        vec_t const& p2  = to_vec( i_points[( i + 1 ) % c] );
        real_t const p1x = get< 0 >( p1 );
        real_t const p1y = get< 1 >( p1 );
        real_t const p2x = get< 0 >( p2 );
        real_t const p2y = get< 1 >( p2 );

        acc = p1x * p2y - p2x * p1y;
        signed_area += acc;
        centroid += ( p1 + p2 ) * acc;
    }

    signed_area *= 0.5;
    centroid /= 6.0 * signed_area;
    return std::make_pair( centroid, signed_area );
}

//--------------------------------
D_CLASS_TEMPLATE
vec_t
    polygon< D_TEMPLATE_ARGS >::to_vec( data_t const& i_data )
{
    return to_vec_t( )( i_data );
}

//--------------------------------
D_CLASS_TEMPLATE
vec_t&&
    polygon< D_TEMPLATE_ARGS >::to_vec( data_t&& i_data )
{
    return to_vec_t( )( std::forward< data_t >( i_data ) );
}

//--------------------------------
D_CLASS_TEMPLATE
template < size_t index >
real_t
    polygon< D_TEMPLATE_ARGS >::get( vec_t const& i_vec )
{
    return get_t( ).template operator( )< index >( i_vec );
}

//--------------------------------
D_CLASS_TEMPLATE
template < typename other_data_t,
           typename other_vec_t,
           typename other_to_vec_t,
           typename other_get_t >
auto
    polygon< D_TEMPLATE_ARGS >::clip(
        polygon< other_data_t, other_vec_t, other_to_vec_t, other_get_t > const&
            i_subject_polygon ) const
{
    static_assert( std::is_same< vec_t, other_vec_t >( ) );
    using other_t    = polygon< other_data_t, other_vec_t, other_to_vec_t, other_get_t >;
    using new_data_t = std::pair< vec_t, bool >;

    auto const&                  subject_polygon = i_subject_polygon.get_data_points( );
    size_t const                 subject_size    = subject_polygon.size( );
    size_t const                 max_size        = subject_size + points_.size( );
    aligned_vector< new_data_t > input_points( max_size );
    aligned_vector< new_data_t > new_points( max_size );
    new_points.resize( subject_size );
    // copy subject polygon to new polygon
    {
        auto func = []( other_data_t const& i_data ) {
            return std::make_pair( other_t::to_vec( i_data ), false );
        };
        std::transform( std::begin( subject_polygon ),
                        std::end( subject_polygon ),
                        std::begin( new_points ),
                        func );
    }

    // for each edge in clip polygon
    for ( size_t j = 0, clip_count = points_.size( ); j < clip_count; ++j )
    {
        //assert( new_points.size( ) > 0 );

        // copy new polygon to input polygon
        input_points.resize( new_points.size( ) );
        std::copy(
            std::begin( new_points ), std::end( new_points ), std::begin( input_points ) );
        new_points.clear( );

        // get clipping polygon edge
        vec_t const& clip1_vec = to_vec( points_[j] );
        vec_t const& clip2_vec = to_vec( points_[( j + 1 ) % clip_count] );

        assert( new_points.size( ) == 0 );
        //assert( input_points.size( ) > 0 );
        for ( size_t i = 0, input_count = input_points.size( ); i < input_count; ++i )
        {
            // get subject polygon edge
            new_data_t&  poly1     = input_points[i];
            new_data_t&  poly2     = input_points[( i + 1 ) % input_count];
            vec_t const& poly1_vec = poly1.first;
            vec_t const& poly2_vec = poly2.first;
            bool const   p1_inside = inside_edge( poly1_vec, clip1_vec, clip2_vec );
            bool const   p2_inside = inside_edge( poly2_vec, clip1_vec, clip2_vec );

            // Case 1: Both vertices are inside:
            // Only the second vertex is added to the output list
            if ( p1_inside && p2_inside )
            {
                new_points.emplace_back( std::move( poly2 ) );
            }
            // Case 2: First vertex is outside while second one is inside:
            // Both the point of intersection of the edge with the clip boundary
            // and the second vertex are added to the output list
            else if ( !p1_inside && p2_inside )
            {
                new_points.emplace_back( std::make_pair(
                    intersection( clip1_vec, clip2_vec, poly1_vec, poly2_vec ), true ) );
                new_points.emplace_back( std::move( poly2 ) );
            }
            // Case 3: First vertex is inside while second one is outside:
            // Only the point of intersection of the edge with the clip boundary
            // is added to the output list
            else if ( p1_inside && !p2_inside )
            {
                new_points.emplace_back( std::make_pair(
                    intersection( clip1_vec, clip2_vec, poly1_vec, poly2_vec ), true ) );
            }
            // Case 4: Both vertices are outside
            else
            {
                // No vertices are added to the output list
                assert( !p1_inside && !p2_inside );
            }
        }
    }

    return polygon< new_data_t, vec2, access::to_vec_pair >( std::move( new_points ) );
}

//--------------------------------
D_CLASS_TEMPLATE
bool
    polygon< D_TEMPLATE_ARGS >::inside_edge( vec_t const& i_p,
                                             vec_t const& i_e1,
                                             vec_t const& i_e2 )
{
    real_t const px  = get< 0 >( i_p );
    real_t const py  = get< 1 >( i_p );
    real_t const e1x = get< 0 >( i_e1 );
    real_t const e1y = get< 1 >( i_e1 );
    real_t const e2x = get< 0 >( i_e2 );
    real_t const e2y = get< 1 >( i_e2 );
    return ( e2y - e1y ) * px + ( e1x - e2x ) * py + ( e2x * e1y - e1x * e2y ) < 0.0;
}

//--------------------------------
D_CLASS_TEMPLATE
vec_t
    polygon< D_TEMPLATE_ARGS >::intersection( vec_t const& cp1,
                                              vec_t const& cp2,
                                              vec_t const& s,
                                              vec_t const& e )
{
    real_t const cp1x = get< 0 >( cp1 );
    real_t const cp1y = get< 1 >( cp1 );
    real_t const cp2x = get< 0 >( cp2 );
    real_t const cp2y = get< 1 >( cp2 );
    real_t const sx   = get< 0 >( s );
    real_t const sy   = get< 1 >( s );
    real_t const ex   = get< 0 >( e );
    real_t const ey   = get< 1 >( e );

    vec2 const   dc  = { cp1x - cp2x, cp1y - cp2y };
    vec2 const   dp  = { sx - ex, sy - ey };
    real_t const dcx = get< 0 >( dc );
    real_t const dcy = get< 1 >( dc );
    real_t const dpx = get< 0 >( dp );
    real_t const dpy = get< 1 >( dp );

    real_t const n1 = cp1x * cp2y - cp1y * cp2x;
    real_t const n2 = sx * ey - sy * ex;
    real_t const n3 = 1.0 / ( dcx * dpy - dcy * dpx );

    return vec_t( vec_t{ ( n1 * dpx - n2 * dcx ) * n3, ( n1 * dpy - n2 * dcy ) * n3 } );
}

#undef D_TEMPLATE_ARGS
#undef D_CLASS_TEMPLATE
