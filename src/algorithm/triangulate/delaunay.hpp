#if !defined( D_DELAUNAY_HPP )
#define D_DELAUNAY_HPP

#include "detail/circumcircle.hpp"
#include "detail/collision_region.hpp"
#include "detail/pointer_edge.hpp"
#include "detail/region.hpp"
#include "detail/triangle.hpp"
#include "detail/value_edge.hpp"

#include <list>
#include <numeric>
#include <optional>
#include <sys_eigen.hpp>
#include <sys_trie.hpp>
#include <unordered_set>

// TODO: change bounding shape to any polygon
namespace algorithm::triangluate
{
using namespace sys;
using namespace tsl;

class delaunay final
{
  private:
  public:
    using triangle_type = detail::triangle;
    using region_type   = detail::region;

    delaunay( );

    void triangulate( aligned_vector< vec2 > const& i_points, geometry::box const& i_region );

    void clear( );

    detail::triangle_cont_t const& triangles( ) const;
    detail::circle_cont_t const&   circumcircles( ) const;
    detail::region_cont_t const&   regions( ) const;

    detail::regions_itr_set_t const& iterate_triangle_regions( detail::triangle const& i_tri );

  private:
    void init( geometry::box const& i_bounds );

    void add_super_triangle( );
    void find_bad_triangles( vec2 const&                  i_new_point,
                             detail::triangle_itr_cont_t& io_bad_triangles );
    void find_polygon( detail::triangle_itr_cont_t const& i_bad_triangles,
                       detail::polygon_edges_cont&        io_polygon );
    void remove_bad_triangles( detail::triangle_itr_cont_t const& i_bad_triangles );
    void retriangulate_hole( detail::polygon_edges_cont const& i_edges,
                             vec2 const&                       i_new_point );

    void add_points( aligned_vector< vec2 > const& i_points );
    void regionulate( aligned_vector< vec2 > const& i_points );
    void update_region( vec2 const& i_point );

    bool lookup_edge( detail::value_edge const&               i_edge,
                      detail::triangle_point_map_t::iterator& io_result );

    template < typename point_map_t, typename data_cont_t, typename edges_t, typename itr_t >
    static void set_edge_in_point_map( data_cont_t&        i_data,
                                       point_map_t&        io_point_map,
                                       edges_t&            io_edges,
                                       itr_t               i_itr,
                                       detail::value_edge& i_edge );

    void add_triangle( vec2 const& i_a, vec2 const& i_b, vec2 const& i_c );
    void remove_bad_triangle( detail::triangle_itr i_itr );
    void remove_triangle( detail::triangle_itr i_itr );

    detail::circle_itr_polygon clip_polygon( detail::circle_itr_polygon const& i_poly,
                                             detail::new_point_map_t& io_new_point_map );

    void add_clipped_region( detail::tri_itr_set_t const&     i_region_triangles,
                             detail::circle_itr_cont_t const& i_out_of_bounds,
                             detail::circle_itr_set_t&        io_seen_circles,
                             vec2 const&                      i_seed,
                             detail::circle_itr_polygon&&     i_poly );

    detail::region_cont_t::iterator add_region( detail::tri_itr_set_t const& i_region_triangles,
                                                vec2 const&                  i_seed,
                                                detail::circle_itr_polygon&& i_poly );

    std::unordered_set< detail::triangle_itr, detail::deref_addressof_hash >
         mark_super_triangles( ) const;
    void remove_marked_triangles(
        std::unordered_set< detail::triangle_itr, detail::deref_addressof_hash > const& i_set );

    void set_back_pointers( );

    void check_memory_error( ) const;
    void print_point_map( ) const;

    template < typename value_type, typename... args_t >
    static auto emplace_back( std::list< value_type >& io_list, args_t&&... o_values );

    //--------------------------------
    detail::triangle_point_map_t
                            tri_point_map_; // maps two points (i.e. an edge) to a edge iterator
    detail::triangle_cont_t triangles_;     // triangle container
    detail::circle_cont_t   circles_;       // circumcirles of triangles container
    detail::tri_edge_cont_t
        tri_edges_; // edge container, each edge has iterators to it's triangles

    detail::region_point_map_t
                               reg_point_map_; // maps two points (i.e. an edge) to a edge iterator
    detail::region_cont_t      regions_;
    detail::region_edge_cont_t reg_edges_;
    detail::regions_itr_set_t  triangle_regions_set_;

    // ad hoc acceleration structure
    detail::collision_region prev_region_;
    detail::collision_region current_region_;

    geometry::triangle  super_triangle_; // a bounding triangle to perform the algorithm in
    geometry::box       bounds_;         // bounding box
    geometry::polygon<> bounds_poly_;    // bounding box as a polygon

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

#include "delaunay.inl"

} // namespace algorithm::triangluate
#endif
