#include "../geometry/comparison.hpp"
#include "jitter_sample.hpp"

#include <cmath>
#include <random>

namespace algorithm::generate
{
class point_pair
{
  public:
    point_pair( vec2&& i_random, vec2&& i_center )
        : random( std::forward< vec2 >( i_random ) )
        , center( std::forward< vec2 >( i_center ) )
    {
    }

    vec2 random;
    vec2 center;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

//--------------------------------
void
    jitter_sample( size_t const            i_subdivision,
                   real_t const            i_pass_rate,
                   geometry::box const&    i_area,
                   std::mt19937_64&        io_generator,
                   aligned_vector< vec2 >& io_random_points,
                   aligned_vector< vec2 >& io_ref_points,
                   vec2&                   io_halfwidths )
{
    assert( i_subdivision > 0 );
    assert( i_pass_rate > 0.0 && i_pass_rate <= 1.0 );
    assert( i_area.min.x( ) < i_area.max.x( ) && i_area.min.y( ) < i_area.max.y( ) );

    io_random_points.clear( );
    io_ref_points.clear( );

    vec2 const   dimensions = i_area.max - i_area.min;
    real_t const width      = dimensions.x( ) / static_cast< real_t >( i_subdivision );
    real_t const height     = dimensions.y( ) / static_cast< real_t >( i_subdivision );
    real_t const halfwidth  = width / 2.0;
    real_t const halfheight = height / 2.0;
    io_halfwidths.x( )      = halfwidth;
    io_halfwidths.y( )      = halfheight;

    std::uniform_real_distribution< real_t > horizontal_distribution( -halfwidth, halfwidth );
    std::uniform_real_distribution< real_t > vertical_distribution( -halfheight, halfheight );
    std::uniform_real_distribution< real_t > skip_distribution( 0.0, 1.0 );

    sys::aligned_vector< point_pair > random_points;

    // for each point in unit split space:
    for ( real_t x = i_area.min.x( ) + halfwidth, x_max = i_area.max.x( ); x <= x_max;
          x += width )
    {
        for ( real_t y = i_area.min.y( ) + halfheight, y_max = i_area.max.y( ); y <= y_max;
              y += height )
        {
            // skip chance
            if ( skip_distribution( io_generator ) >= i_pass_rate )
            {
                // skip point
                continue;
            }

            vec2 center( x, y );
            vec2 random_point = center + vec2( horizontal_distribution( io_generator ),
                                               vertical_distribution( io_generator ) );

            random_points.emplace_back( std::move( random_point ), std::move( center ) );
        }
    }

    auto compare = []( point_pair const& p1, point_pair const& p2 ) {
        real_t const& x1 = p1.random.x( );
        real_t const& y1 = p1.random.y( );
        real_t const& x2 = p2.random.x( );
        real_t const& y2 = p2.random.y( );
        if ( geometry::nearly_equal( x1, x2 ) == false )
        {
            return x1 < x2;
        }
        else
        {
            return y1 < y2;
        }
    };

    std::sort( std::begin( random_points ), std::end( random_points ), compare );
    for ( auto& pair : random_points )
    {
        io_random_points.emplace_back( std::move( pair.random ) );
        io_ref_points.emplace_back( std::move( pair.center ) );
    }
}

} // namespace algorithm::generate
