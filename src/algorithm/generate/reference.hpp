#if !defined( D_REFERENCE_HPP )
#define D_REFERENCE_HPP
#include "../geometry/shapes.hpp"

#include <sys_eigen.hpp>

namespace algorithm::generate
{
using namespace sys;

void reference( geometry::box const& i_area, aligned_vector< vec2 >& io_points );

} // namespace algorithm::generate

#endif
