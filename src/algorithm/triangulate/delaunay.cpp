#include "delaunay.hpp"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>

namespace algorithm::triangluate
{
using namespace detail;

//--------------------------------
delaunay::delaunay( )
{
    tri_point_map_.max_load_factor( 2.0 );
    tri_point_map_.burst_threshold( 1 << 11 );
}

//--------------------------------
void
    delaunay::init( geometry::box const& i_bounds )
{
    tri_point_map_.clear( );
    triangles_.clear( );
    circles_.clear( );
    tri_edges_.clear( );
    reg_point_map_.clear( );
    regions_.clear( );
    reg_edges_.clear( );
    triangle_regions_set_.clear( );
    bounds_poly_.clear_points( );

    prev_region_.clear_triangles( );
    current_region_.clear_triangles( );

    vec2 const   bottom_left  = i_bounds.min;                                 // bottom_left
    vec2 const   top_left     = vec2( i_bounds.min.x( ), i_bounds.max.y( ) ); // top_left
    vec2 const   top_right    = i_bounds.max;                                 // top_right
    vec2 const   bottom_right = vec2( i_bounds.max.x( ), i_bounds.min.y( ) ); // bottom_right
    real_t const height       = i_bounds.max.y( ) - i_bounds.min.y( );

    current_region_.get_min_x( ) = bottom_left.x( );
    current_region_.get_max_x( ) = bottom_left.x( ) + std::numeric_limits< real_t >::epsilon( );

    // giant triangle (when bounds is a square it will be an equilateral triangle)
    super_triangle_.a( ) = bottom_left;
    super_triangle_.a( ).x( ) -= height;
    super_triangle_.b( ) = ( top_left + top_right ) / 2.0; // top center
    super_triangle_.b( ).y( ) += height;
    super_triangle_.c( ) = bottom_right;
    super_triangle_.c( ).x( ) += height;

    bounds_.min = bottom_left;
    bounds_.max = top_right;

    bounds_poly_.add_point( bottom_left );
    bounds_poly_.add_point( top_left );
    bounds_poly_.add_point( top_right );
    bounds_poly_.add_point( bottom_right );
    bounds_poly_.ccw_order_points( );
}

//--------------------------------
void
    delaunay::add_triangle( vec2 const& i_a, vec2 const& i_b, vec2 const& i_c )
{
    triangle::point_ptrs_t points{ &i_a, &i_b, &i_c };

    assert( circles_.size( ) == triangles_.size( ) );
    auto circumcircle_itr =
        emplace_back( circles_,
                      std::end( triangles_ ),
                      geometry::triangle( i_a, i_b, i_c ).create_circumcircle( ) );
    auto tri_itr =
        emplace_back( triangles_, *points[0], *points[1], *points[2], circumcircle_itr );

    auto& tri = *tri_itr;

    auto value_edges = tri.create_value_edges( );
    for ( auto& value_edge : value_edges )
    {
        set_edge_in_point_map( triangles_, tri_point_map_, tri_edges_, tri_itr, value_edge );
    }

    // all new triangles are added to the current region
    current_region_.add_triangle( tri_itr );
}

//--------------------------------
void
    delaunay::remove_bad_triangle( triangle_itr i_itr )
{
    auto  tri_itr = i_itr;
    auto& tri     = *i_itr;

    auto                           value_edges = tri.create_value_edges( );
    triangle_point_map_t::iterator result_itr;
    triangle_itr                   end = std::end( triangles_ );
    for ( auto const& e : value_edges )
    {
        vec2 const a( e.a( ) );
        vec2 const b( e.b( ) );
        value_edge edge( a, b );
        if ( lookup_edge( edge, result_itr ) )
        {
            auto& result = **result_itr;
            // erase edges which have two bad neighbors
            if ( ( result[0] != end && result[0]->get_bad( ) ) &&
                 ( result[1] != end && result[1]->get_bad( ) ) )
            {
                tri_edges_.erase( *result_itr );
                tri_point_map_.erase_ks( edge.get_ptr( ), edge.size( ) );
                // erase reverse edge
                value_edge edge_reversed( b, a );
                tri_point_map_.erase_ks( edge_reversed.get_ptr( ), edge_reversed.size( ) );
                continue;
            }

            if ( result[1] != end && result[1] == tri_itr )
            {
                assert( ( *tri_point_map_.at_ks( edge.get_ptr( ), edge.size( ) ) )[1] != end );
                result[1] = end;
                assert( ( *tri_point_map_.at_ks( edge.get_ptr( ), edge.size( ) ) )[1] == end );
            }
            else if ( result[0] != end && result[0] == tri_itr )
            {
                assert( ( *tri_point_map_.at_ks( edge.get_ptr( ), edge.size( ) ) )[0] !=
                        ( *tri_point_map_.at_ks( edge.get_ptr( ), edge.size( ) ) )[1] );
                result[0] = result[1];
                assert( ( *tri_point_map_.at_ks( edge.get_ptr( ), edge.size( ) ) )[0] ==
                        ( *tri_point_map_.at_ks( edge.get_ptr( ), edge.size( ) ) )[1] );
                result[1] = end;
                assert( ( *tri_point_map_.at_ks( edge.get_ptr( ), edge.size( ) ) )[1] == end );
            }
        }
        // it is acceptable to not find the edge
        // because it was erased for having two bad triangles next to it
    }

    // deleted triangles MUST be in the previous region (there is no other way they can collide)
    prev_region_.remove_triangle( tri_itr );

    circles_.erase( tri_itr->circumcircle_itr( ) );
    triangles_.erase( tri_itr );
    assert( circles_.size( ) == triangles_.size( ) );
}

//--------------------------------
void
    delaunay::remove_triangle( triangle_itr i_itr )
{
    auto  tri_itr = i_itr;
    auto& tri     = *i_itr;

    auto                           value_edges = tri.create_value_edges( );
    triangle_point_map_t::iterator result_itr;
    triangle_itr                   end = std::end( triangles_ );
    for ( auto const& e : value_edges )
    {
        vec2 const a( e.a( ) );
        vec2 const b( e.b( ) );
        value_edge edge( a, b );
        if ( lookup_edge( edge, result_itr ) )
        {
            auto& result = **result_itr;

            if ( result[1] != end && result[1] == tri_itr )
            {
                result[1] = end;
            }
            else if ( result[0] != end && result[0] == tri_itr )
            {
                result[0] = result[1];
                if ( result[1] == end )
                {
                    tri_edges_.erase( *result_itr );
                    tri_point_map_.erase_ks( edge.get_ptr( ), edge.size( ) );
                    value_edge edge_reversed( b, a );
                    tri_point_map_.erase_ks( edge_reversed.get_ptr( ), edge.size( ) );
                }
                else
                {
                    result[1] = end;
                }
            }
        }
        // it is acceptable to not find the edge
        // because it was erased for having two bad triangles next to it
    }

    circles_.erase( tri_itr->circumcircle_itr( ) );
    triangles_.erase( tri_itr );
    // assert( circles_.size( ) == triangles_.size( ) );
}

//--------------------------------
void
    delaunay::add_super_triangle( )
{
    add_triangle( super_triangle_.a( ), super_triangle_.b( ), super_triangle_.c( ) );
}

//--------------------------------
void
    delaunay::find_bad_triangles( vec2 const&                  i_new_point,
                                  detail::triangle_itr_cont_t& io_bad_triangles )
{
    io_bad_triangles.clear( );
    for ( auto tri_itr = std::begin( prev_region_.get_triangles( ) ),
               end     = std::end( prev_region_.get_triangles( ) );
          tri_itr != end;
          ++tri_itr )
    {
        auto& tri = ( **tri_itr );
        if ( tri.collide_circumcircle( i_new_point ) )
        {
            // bad triangle
            tri.set_bad( );

            io_bad_triangles.emplace_back( *tri_itr );
        }
    }
}

//--------------------------------
void
    delaunay::remove_bad_triangles( triangle_itr_cont_t const& i_bad_triangles )
{
    for ( auto& tri_itr : i_bad_triangles )
    {
        remove_bad_triangle( tri_itr );
    }
}

//--------------------------------
bool
    delaunay::lookup_edge( value_edge const& i_edge, triangle_point_map_t::iterator& io_result )
{
    auto itr = tri_point_map_.find_ks( i_edge.get_ptr( ), i_edge.size( ) );
    if ( itr != tri_point_map_.end( ) )
    {
        io_result = itr;
        return true;
    }
    return false;
}

//--------------------------------
void
    delaunay::find_polygon( detail::triangle_itr_cont_t const& i_bad_triangles,
                            detail::polygon_edges_cont&        io_polygon )
{
    io_polygon.clear( );
    auto                           end = std::end( triangles_ );
    triangle_point_map_t::iterator result_itr;
    for ( auto const& tri_itr : i_bad_triangles )
    {
        auto const& tri = ( *tri_itr );

        auto const pointer_edges = tri.create_pointer_edges( );
        for ( auto const& ptr_edge : pointer_edges )
        {
            value_edge edge( ptr_edge.a( ), ptr_edge.b( ) );
            if ( lookup_edge( edge, result_itr ) )
            {
                auto& result = **result_itr;
                // if any of the other triangles are not bad
                for ( auto const& other_tri : result )
                {
                    if ( other_tri == end || other_tri->get_bad( ) == false )
                    {
                        // then we have an edge for the outer wall of the polygon
                        io_polygon.emplace_back( ptr_edge );
                        break;
                    }
                }
            }
        }
    }
}

//--------------------------------
void
    delaunay::retriangulate_hole( detail::polygon_edges_cont const& i_edges,
                                  vec2 const&                       i_new_point )
{
    for ( auto const& ptr_edge : i_edges )
    {
        add_triangle( ptr_edge.a( ), ptr_edge.b( ), i_new_point );
    }
}

//--------------------------------
void
    delaunay::update_region( vec2 const& i_point )
{
    // bring forward any circles which overlap previous region and current region
    current_region_ += prev_region_;
    prev_region_ = current_region_;

    // create new region
    real_t const old_max = prev_region_.get_max_x( );
    real_t const dist    = i_point.x( ) - old_max;
    assert( dist >= 0.0 );

    current_region_.clear_triangles( );
    if ( dist > 0.5 )
    {
        current_region_.get_min_x( ) = old_max;
        current_region_.get_max_x( ) = old_max + dist;
    }
    else
    {
        current_region_.get_max_x( ) += std::abs( dist );
    }
}

//--------------------------------
void
    delaunay::add_points( aligned_vector< vec2 > const& i_points )
{
    triangle_itr_cont_t bad_triangles;
    polygon_edges_cont  polygon;

    for ( vec2 const& p : i_points )
    {
        update_region( p );
        find_bad_triangles( p, bad_triangles );
        find_polygon( bad_triangles, polygon );
        remove_bad_triangles( bad_triangles );
        retriangulate_hole( polygon, p );
    }
}

//--------------------------------
void
    delaunay::triangulate( aligned_vector< vec2 > const& i_points,
                           geometry::box const&          i_bounds )
{
    // clean previous str_key and setup bounds str_key
    init( i_bounds );

    // create outer most triangle
    add_super_triangle( );

    // add all points incrementally to super triangle
    add_points( i_points );

    // mark triangles to be removed (from super triangle)
    auto set = mark_super_triangles( );

    // generate voronoi regions
    regionulate( i_points );

    remove_marked_triangles( set );

    // set back pointers on circumcircles
    set_back_pointers( );

    // check_memory_error();
}
//----------------------------------------------------------------

//--------------------------------
void
    delaunay::regionulate( aligned_vector< vec2 > const& i_points )
{
    tri_itr_set_t     set;
    new_point_map_t   new_point_map;
    circle_itr_set_t  seen_circles; // TODO: review / change algorithm
    circle_itr_cont_t out_of_bounds;

    for ( vec2 const& p : i_points )
    {
        set.clear( );
        out_of_bounds.clear( );

        value_edge half_edge( p );
        auto const range =
            tri_point_map_.equal_prefix_range_ks( half_edge.get_ptr( ), half_edge.size( ) );
        // for each surrounding point
        for ( auto itr = range.first, end = range.second; itr != end; ++itr )
        {
            auto const& value = *itr.value( );
            assert( value[0] != triangles_.end( ) );
            assert( value[1] != triangles_.end( ) );
            set.insert( value[0] );
            set.insert( value[1] );
        }

        auto poly = circle_itr_polygon( );
        for ( triangle_itr const& itr : set )
        {
            circle_itr  circle = itr->circumcircle_itr( );
            vec2 const& point  = circle->p;
            poly.add_point( circle );
            if ( box_point_collision( bounds_, point ) == false )
            {
                out_of_bounds.emplace_back( circle );
            }
        }
        poly.ccw_order_points( );

        bool const is_out_of_bounds = out_of_bounds.size( ) > 0;
        if ( is_out_of_bounds )
        {
            auto new_poly = clip_polygon( poly, new_point_map );
            add_clipped_region( set, out_of_bounds, seen_circles, p, std::move( new_poly ) );
        }
        else
        {
            add_region( set, p, std::move( poly ) );
        }
    }
}

//--------------------------------
detail::circle_itr_polygon
    delaunay::clip_polygon( detail::circle_itr_polygon const& i_poly,
                            detail::new_point_map_t&          io_new_point_map )
{
    real_t constexpr tolerance = 1.0e-10;

    auto clipped_polygon = bounds_poly_.clip( i_poly );
    clipped_polygon.ccw_order_points( );
    // TODO: profile performance difference, points that come out of clipping are rotated but in CCW order

    auto const& clipped_points = clipped_polygon.get_data_points( );
    auto        new_poly       = circle_itr_polygon( );

    auto const&  prev_points     = i_poly.get_data_points( );
    size_t const prev_point_size = prev_points.size( );

    for ( size_t i = 0, j = 0, c = clipped_points.size( ); i < c; )
    {
        bool new_point = clipped_points[i].second;
        auto point     = clipped_points[i].first;
        assert( point.x( ) + tolerance >= bounds_.min.x( ) );
        assert( point.x( ) - tolerance <= bounds_.max.x( ) );
        assert( point.y( ) + tolerance >= bounds_.min.y( ) );
        assert( point.y( ) - tolerance <= bounds_.max.y( ) );
        // TODO: assert this is desired, it adds precision to the region areas
        point.x( ) = std::clamp( point.x( ), bounds_.min.x( ), bounds_.max.x( ) );
        point.y( ) = std::clamp( point.y( ), bounds_.min.y( ), bounds_.max.y( ) );
        if ( new_point )
        {
            // check it hasn't been seen before
            auto res = io_new_point_map.find( point );
            if ( res == std::end( io_new_point_map ) )
            {
                // put a circumcirle into the circles so polygon has an iterator to point at
                auto new_itr = emplace_back( circles_, std::end( triangles_ ), point, 0.0 );
                io_new_point_map.insert( std::make_pair( point, new_itr ) );
                new_poly.add_point( new_itr );
            }
            else
            {
                new_poly.add_point( res->second );
            }
            ++i;
        }
        else
        {
            j %= prev_point_size;
            if ( clipped_points[i].first == prev_points[j]->p )
            {
                new_poly.add_point( prev_points[j] );
                // increment both i and j
                ++i;
            }
            // else go around again, i it must match something (within less than poly.size() increments)
            ++j;
        }
    }

    return new_poly; // RVO
}

//--------------------------------
void
    delaunay::add_clipped_region( detail::tri_itr_set_t const&     i_region_triangles,
                                  detail::circle_itr_cont_t const& i_out_of_bounds,
                                  detail::circle_itr_set_t&        io_seen_circles,
                                  vec2 const&                      i_seed,
                                  detail::circle_itr_polygon&&     i_poly )
{
    detail::triangle_itr_cont_t tri_vec;
    for ( auto itr : i_region_triangles )
    {
        if ( itr->get_bad( ) == false )
        {
            tri_vec.emplace_back( itr );
        }
    }

    assert( i_out_of_bounds.size( ) > 0 );
    auto new_region = emplace_back( regions_,
                                    i_seed,
                                    true,
                                    std::forward< detail::circle_itr_polygon >( i_poly ),
                                    std::forward< detail::triangle_itr_cont_t >( tri_vec ) );

    auto&       reg         = *new_region;
    auto const& circle_itrs = reg.get_polygon( ).get_data_points( );

    detail::value_edge edge;
    for ( size_t i = 0, c = circle_itrs.size( ); i < c; ++i )
    {
        edge.reset( circle_itrs[i]->p, circle_itrs[( i + 1 ) % c]->p );
        set_edge_in_point_map( regions_, reg_point_map_, reg_edges_, new_region, edge );
    }

    // each circumcirle that is out of bounds will not have been in clipped polygon
    // which means it can never be looked up in reg_point_map_
    // therefore manually add it below (while avoiding duplicates via seen_circles)
    value_edge e;
    for ( auto itr : i_out_of_bounds )
    {
        auto res = io_seen_circles.insert( itr );
        if ( res.second == true )
        {
            e.reset( itr->p );
            set_edge_in_point_map( regions_, reg_point_map_, reg_edges_, new_region, e );
        }
    }
}

//--------------------------------
detail::region_cont_t::iterator
    delaunay::add_region( detail::tri_itr_set_t const& i_region_triangles,
                          vec2 const&                  i_seed,
                          detail::circle_itr_polygon&& i_poly )
{
    triangle_itr_cont_t tri_vec;
    for ( auto itr : i_region_triangles )
    {
        assert( itr->get_bad( ) == false );
        tri_vec.emplace_back( itr );
    }

    auto reg_itr = emplace_back( regions_,
                                 i_seed,
                                 false,
                                 std::forward< detail::circle_itr_polygon >( i_poly ),
                                 std::forward< detail::triangle_itr_cont_t >( tri_vec ) );

    auto&       reg         = *reg_itr;
    auto const& circle_itrs = reg.get_polygon( ).get_data_points( );

    detail::value_edge edge;
    for ( size_t i = 0, c = circle_itrs.size( ); i < c; ++i )
    {
        edge.reset( circle_itrs[i]->p, circle_itrs[( i + 1 ) % c]->p );
        set_edge_in_point_map( regions_, reg_point_map_, reg_edges_, reg_itr, edge );
    }
    return reg_itr;
}

//--------------------------------
std::unordered_set< detail::triangle_itr, detail::deref_addressof_hash >
    delaunay::mark_super_triangles( ) const
{
    std::unordered_set< detail::triangle_itr, detail::deref_addressof_hash > set;
    for ( vec2 const& p : super_triangle_.points )
    {
        value_edge half_edge( p ); // a.k.a a point
        auto const range =
            tri_point_map_.equal_prefix_range_ks( half_edge.get_ptr( ), half_edge.size( ) );
        for ( auto itr = range.first, end = range.second; itr != end; ++itr )
        {
            auto const& value = *itr.value( );
            if ( value[0] != triangles_.end( ) )
            {
                set.insert( value[0] );
            }
            if ( value[1] != triangles_.end( ) )
            {
                set.insert( value[1] );
            }
        }
    }

    for ( auto const& tri : set )
    {
        assert( box_point_collision( bounds_, tri->circumcircle( ).p ) == false );
        tri->set_bad( );
    }

    return set; // RVO
}

//--------------------------------
void
    delaunay::remove_marked_triangles(
        std::unordered_set< detail::triangle_itr, detail::deref_addressof_hash > const& i_set )
{
    for ( auto const& tri : i_set )
    {
        remove_triangle( tri );
    }
}

//--------------------------------
void
    delaunay::set_back_pointers( )
{
    for ( auto itr = std::begin( triangles_ ), end = std::end( triangles_ ); itr != end; ++itr )
    {
        auto const& tri                              = *itr;
        tri.circumcircle_itr( )->source_triangle_itr = itr;
    }
}

//--------------------------------
void
    delaunay::clear( )
{
    tri_point_map_.clear( );
    triangles_.clear( );
    circles_.clear( );
    tri_edges_.clear( );
    reg_point_map_.clear( );
    regions_.clear( );
    reg_edges_.clear( );
    bounds_poly_.clear_points( );
}

//--------------------------------
triangle_cont_t const&
    delaunay::triangles( ) const
{
    return triangles_;
}

//--------------------------------
circle_cont_t const&
    delaunay::circumcircles( ) const
{
    return circles_;
}

//--------------------------------
region_cont_t const&
    delaunay::regions( ) const
{
    return regions_;
}

//--------------------------------
detail::regions_itr_set_t const&
    delaunay::iterate_triangle_regions( triangle const& i_tri )
{
    value_edge half_edge( i_tri.circumcircle( ).p ); // a.k.a a point
    auto const range =
        reg_point_map_.equal_prefix_range_ks( half_edge.get_ptr( ), half_edge.size( ) );
    triangle_regions_set_.clear( );
    auto itr = range.first;
    auto end = range.second;
    while ( itr != end )
    {
        auto const& region_itr_array = *itr.value( );
        auto const  itr1             = region_itr_array.at( 0 );
        auto const  itr2             = region_itr_array.at( 1 );
        assert( itr1 != std::end( regions_ ) );
        triangle_regions_set_.insert( itr1 );
        if ( itr2 != std::end( regions_ ) )
        {
            triangle_regions_set_.insert( itr2 );
        }
        ++itr;
    }

    return triangle_regions_set_;
}

//--------------------------------
// intended for debug usage
void
    delaunay::check_memory_error( ) const
{
    auto        tri_end = triangles_.end( );
    size_t      i       = 0;
    auto        itr     = tri_point_map_.begin( );
    auto        end     = tri_point_map_.end( );
    std::string key;
    while ( itr != end )
    {
        itr.key( key );
        auto const& value = *itr.value( );
        i += key.size( );
        i += value.size( );

        auto const range =
            tri_point_map_.equal_prefix_range_ks( key.data( ), sizeof( real_t ) );
        size_t count = 0;
        for ( auto ptr = range.first, ptr_end = range.second; ptr != ptr_end; ++ptr )
        {
            ++count;
        }
        i += count;

        for ( auto& tri_itr : value )
        {
            if ( tri_itr != tri_end )
            {
                auto&      tri = *tri_itr;
                vec2 const abc = tri.a( ) + tri.b( ) + tri.c( );
                i += static_cast< size_t >( abc.x( ) );
                i += static_cast< size_t >( abc.y( ) );
            }
        }
        ++itr;
    }
    std::cout << i << ", ";
}

//--------------------------------
// intended for debug usage, prints the contents of point_map
void
    delaunay::print_point_map( ) const
{
    size_t            count   = 0;
    auto              tri_end = triangles_.end( );
    std::stringstream s;
    std::string       str_key;
    for ( auto itr = tri_point_map_.begin( ), end = tri_point_map_.end( ); itr != end; ++itr )
    {
        itr.key( str_key );
        assert( str_key.size( ) >= ( sizeof( real_t ) * 4 ) );
        real_t key[4];
        memcpy( &key[0], str_key.data( ), sizeof( real_t ) * 4 );

        s << "edge" << count << ": (" << key[0] << "," << key[1] << ")->(" << key[2] << ","
          << key[3] << ")\n";
        ++count;

        auto& entry = *itr.value( );
        for ( auto& tri_itr : entry )
        {
            if ( tri_itr != tri_end )
            {
                auto&        tri = *tri_itr;
                real_t const x1  = tri.a( ).x( );
                real_t const y1  = tri.a( ).y( );
                real_t const x2  = tri.b( ).x( );
                real_t const y2  = tri.b( ).y( );
                real_t const x3  = tri.c( ).x( );
                real_t const y3  = tri.c( ).y( );
                s << "\ttri: " << std::hex << size_t( &tri ) << std::dec << " {";
                s << "(" << x1 << "," << y1 << "),(" << x2 << "," << y2 << "),(" << x3 << ","
                  << y3 << ")}\n";
            }
            else
            {
                s << "\ttri: {null}\n";
            }
        }
    }
    std::cout << s.str( );
}

} // namespace algorithm::triangluate
