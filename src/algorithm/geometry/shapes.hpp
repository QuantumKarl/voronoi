#if !defined( D_SHAPES_HPP )
#define D_SHAPES_HPP
#include <array>
#include <sys_eigen.hpp>

namespace algorithm::geometry
{
using namespace sys;

//--------------------------------
// min / max point box
class box final
{
  public:
    box( ) noexcept;
    box( vec2 const& i_min, vec2 const& i_max ) noexcept;
    box( vec2&& i_min, vec2&& i_max ) noexcept;

    vec2 min; // bottom left of the box
    vec2 max; // top right of the box

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

//--------------------------------
class circle
{
  public:
    circle( ) noexcept;
    circle( vec2 const& i_v, real_t const i_radius ) noexcept;
    circle( vec2&& i_v, real_t const i_radius ) noexcept;

    bool point_collision( vec2 const& i_point ) const;

    vec2   p; // point / center / origin
    real_t r; // radius

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

//--------------------------------
class triangle final
{
  public:
    triangle( ) noexcept;
    triangle( vec2 const& i_a, vec2 const& i_b, vec2 const& i_c ) noexcept;
    triangle( vec2&& i_a, vec2&& i_b, vec2&& i_c ) noexcept;

    circle create_circumcircle( ) const;

    vec2& a( );
    vec2& b( );
    vec2& c( );

    vec2 const& a( ) const;
    vec2 const& b( ) const;
    vec2 const& c( ) const;

    aligned_vector< vec2 > points;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

//--------------------------------
class ray final
{
  public:
    ray( ) noexcept;
    ray( vec2 const& i_o, vec2 const& i_dir ) noexcept;
    ray( vec2&& i_o, vec2&& i_dir ) noexcept;

    vec2& o( );
    vec2& d( );

    vec2 const& o( ) const;
    vec2 const& d( ) const;

    vec2 const& inv( ) const;
    void        update_inv( );

  private:
    vec2 o_;   // origin
    vec2 d_;   // direction
    vec2 inv_; // 1.0 / dir

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

//--------------------------------
// collisions
bool box_ray_collision( box b, ray const& r, real_t& o_tmin, real_t& o_tmax );

bool box_circle_collision( box const& i_box, circle const& i_s );

bool box_point_collision( box const& i_box, vec2 const& i_point );

//--------------------------------
// convenience functions

//--------------------------------
/* from a to b
*/
ray points_to_ray( vec2 const& a, vec2 const& b );

} // namespace algorithm::geometry
#endif
