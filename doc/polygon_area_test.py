# -*- coding: utf-8 -*-
import sys
import math
pi = math.pi

#def calc_area(p):
#    acc = 0.0
#    edges = segments(p)
#    for ((x0, y0), (x1, y1)) in edges:
#        acc += x0*y1 - x1*y0
#    return abs( acc ) / 2.0

#def segments(p):
#    return zip(p, p[1:] + [p[0]])

#https://stackoverflow.com/questions/451426/how-do-i-calculate-the-area-of-a-2d-polygon
def calc_area(p):
    acc = 0.0
    size = len(p)
    for i in range(0,size):
        x0, y0 = p[i]
        x1, y1 = p[(i+1)%size]
        acc += x0*y1 - x1*y0
    return abs( acc ) / 2.0

#https://stackoverflow.com/questions/8487893/generate-all-the-points-on-the-circumference-of-a-circle
def generate_circle(n,r=1.0):
    points = []
    for x in range(0,n):
        points.append( (math.cos(2*pi/n*x)*r, math.sin(2*pi/n*x)*r) ) # X, Y
    return points

#https://en.wikipedia.org/wiki/Regular_polygon
def regular_polyon_area(n):
    return (n/2.0)*math.sin( (2.0*pi) / n )

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    #--------------------------------------------------------------------------
    for i in range(3,100):
        calcuated = calc_area(generate_circle(i))
        exsact = regular_polyon_area(i)
        delta = max(calcuated,exsact) - min(calcuated,exsact)
        print(delta)

if __name__ == "__main__":
    main()