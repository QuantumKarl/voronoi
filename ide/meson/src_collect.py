# -*- coding: utf-8 -*-

#-------------------- source collect ------------------------
# This python script is called from the meson build script.
# It is a crossplaform and simple way of "globbing" for files.

import os
import sys

def run(path,fileTypes):
    for root, dirs, files in os.walk(path):
        for file in files:
            if( any( map( lambda ext : file.endswith(ext), fileTypes ) ) ):
                print ( os.path.join(root, file).replace('\\', '/') )

def main(argv):
    if len(argv) >= 2:
        folder = argv[1]
        fileTypes = argv[1:] 
        run( folder, fileTypes )

if __name__ == '__main__':
    main(sys.argv)
