#include "dialog.hpp"

#include "entry/cmd.h"
#include "entry/entry.h"
#include <bx/math.h>
#include <bx/string.h>
#include <bx/timer.h>

dialog::dialog( ) noexcept
    : values_( )
    , generate_jitter_points_( )
    , generate_hex_points_( )
    , generate_fib_points_( )
    , triangulate_points_( )
    , resource_color_( 0.5f, 0.5f, 0.5f, 1.0f )
    , points_colour_{ 0.8, 0.225, 0.128 }
    , triangle_colour_{ 0.400, 0.380, 0.1 }
    , circumcircle_colour_{ 0.33, 0.33, 0.33 }
    , region_colour_{ 0.0, 0.78, 0.575 }
    , clipped_region_colour_{ 0.0, 0.73, 0.18 }
    , centroid_colour_{ 0.700, 0.050, 0.745 }
    , region_fill_colour_{ 0.06, 0.04, 0.31 }
    , app_( nullptr )
    , sample_index_( 0 )
    , min_( 0.0f )
    , max_( 0.0f )
    , avg_( 0.0f )
    , points_grid_count_( 6 )
    , generator_pass_rate_( 1.0f / 3.0f )
    , fib_points_count_( 32 * 4 + 1 )
    , fib_scale_( 28.0 )
    , debug_box_( true )
    , render_points_( true )
    , render_ref_points_( false )
    , render_circumcircles_( false )
    , render_bounds_( false )
    , render_delaunay_( true )
    , render_regions_( true )
    , render_centroid_( true )
    , render_region_fill_( false )
    , clear_points_( false )
    , show_stats_( false )
{
    values_.fill( 0 );
}

void
    dialog::push_sample( float value )
{
    uint32_t const values_count = static_cast< uint32_t >( values_.size( ) );
    values_[sample_index_]      = value;
    sample_index_               = ( sample_index_ + 1 ) % values_count;

    float min = bx::kFloatMax;
    float max = -bx::kFloatMax;
    float avg = 0.0f;

    for ( uint32_t ii = 0; ii < values_count; ++ii )
    {
        const float val = values_[ii];
        min             = bx::min( min, val );
        max             = bx::max( max, val );
        avg += val;
    }

    min_ = min;
    max_ = max;
    avg_ = avg / values_count;
}

bool
    dialog::bar( float i_width, float i_max_width, float i_height, const ImVec4& i_color )
{
    const ImGuiStyle& style = ImGui::GetStyle( );

    ImVec4 hoveredColor( i_color.x + i_color.x * 0.1f,
                         i_color.y + i_color.y * 0.1f,
                         i_color.z + i_color.z * 0.1f,
                         i_color.w + i_color.w * 0.1f );

    ImGui::PushStyleColor( ImGuiCol_Button, i_color );
    ImGui::PushStyleColor( ImGuiCol_ButtonHovered, hoveredColor );
    ImGui::PushStyleColor( ImGuiCol_ButtonActive, i_color );
    ImGui::PushStyleVar( ImGuiStyleVar_FrameRounding, 0.0f );
    ImGui::PushStyleVar( ImGuiStyleVar_ItemSpacing, ImVec2( 0.0f, style.ItemSpacing.y ) );

    bool item_hovered = false;

    ImGui::Button( "", ImVec2( i_width, i_height ) );
    item_hovered |= ImGui::IsItemHovered( );

    ImGui::SameLine( );
    ImGui::InvisibleButton( "", ImVec2( bx::max( 1.0f, i_max_width - i_width ), i_height ) );
    item_hovered |= ImGui::IsItemHovered( );

    ImGui::PopStyleVar( 2 );
    ImGui::PopStyleColor( 3 );

    return item_hovered;
}

void
    dialog::resource_bar( const char*   i_name,
                          const char*   i_tooltip,
                          uint32_t      i_num,
                          uint32_t      i_max,
                          float         i_max_width,
                          float         i_height,
                          const ImVec4& i_resource_color )
{
    bool item_hovered = false;

    ImGui::Text( "%s: %4d / %4d", i_name, i_num, i_max );
    item_hovered |= ImGui::IsItemHovered( );
    ImGui::SameLine( );

    const float percentage = float( i_num ) / float( i_max );

    item_hovered |= bar(
        bx::max( 1.0f, percentage * i_max_width ), i_max_width, i_height, i_resource_color );
    ImGui::SameLine( );

    ImGui::Text( "%5.2f%%", percentage * 100.0f );

    if ( item_hovered )
    {
        ImGui::SetTooltip( "%s %5.2f%%", i_tooltip, percentage * 100.0f );
    }
}

void
    dialog::draw_dialog( const char* i_error_text )
{
    char temp[1024];
    bx::snprintf( temp, BX_COUNTOF( temp ), "%s", app_->getName( ) );

    ImGui::SetNextWindowPos( ImVec2( 4.0f, 4.0f ), ImGuiCond_FirstUseEver );
    ImGui::SetNextWindowSize( ImVec2( 240.0f, 600.0f ), ImGuiCond_FirstUseEver );

    ImGui::Begin( temp );

    program_header( i_error_text );
    triangulation_header( );

    ImGui::End( );
}

bool
    dialog::get_debug_box( ) const
{
    return debug_box_;
}

bool
    dialog::get_clear_points( )
{
    auto temp     = clear_points_;
    clear_points_ = false;
    return temp;
}

bool
    dialog::get_render_points( ) const
{
    return render_points_;
}

dialog::colour3_t const&
    dialog::get_points_colour( ) const
{
    return points_colour_;
}

bool
    dialog::get_render_ref_points( ) const
{
    return render_ref_points_;
}

bool
    dialog::get_render_circumcircles( ) const
{
    return render_circumcircles_;
}
dialog::colour3_t const&
    dialog::get_circumcircle_colour( ) const
{
    return circumcircle_colour_;
}

bool
    dialog::get_render_bounds( ) const
{
    return render_bounds_;
}

bool
    dialog::get_render_delaunay( ) const
{
    return render_delaunay_;
}

dialog::colour3_t const&
    dialog::get_delaunay_colour( ) const
{
    return triangle_colour_;
}

bool
    dialog::get_render_regions( ) const
{
    return render_regions_;
}

dialog::colour3_t const&
    dialog::get_region_colour( ) const
{
    return region_colour_;
}

dialog::colour3_t const&
    dialog::get_clipped_region_colour( ) const
{
    return clipped_region_colour_;
}

bool
    dialog::get_render_centroid( ) const
{
    return render_centroid_;
}

dialog::colour3_t const&
    dialog::get_centroid_colour( ) const
{
    return centroid_colour_;
}

bool
    dialog::get_render_region_fill( ) const
{
    return render_region_fill_;
}

dialog::colour3_t const&
    dialog::get_region_fill_colour( ) const
{
    return region_fill_colour_;
}

void
    dialog::program_header( const char* i_error_text )
{
    if ( ImGui::CollapsingHeader( "Program" ) )
    {
        ImGui::TextWrapped( "%s", app_->getDescription( ) );
        ImGui::Separator( );

        if ( NULL != i_error_text )
        {
            const int64_t now  = bx::getHPCounter( );
            const int64_t freq = bx::getHPFrequency( );
            const float   time = float( now % freq ) / float( freq );

            bool blink = time > 0.5f;

            ImGui::PushStyleColor( ImGuiCol_Text,
                                   blink ? ImVec4( 1.0, 0.0, 0.0, 1.0 )
                                         : ImVec4( 1.0, 1.0, 1.0, 1.0 ) );
            ImGui::TextWrapped( "%s", i_error_text );
            ImGui::Separator( );
            ImGui::PopStyleColor( );
        }

        {
            const bgfx::Caps* caps = bgfx::getCaps( );
            if ( 0 != ( caps->supported & BGFX_CAPS_GRAPHICS_DEBUGGER ) )
            {
                ImGui::SameLine( );
                ImGui::Text( ICON_FA_SNOWFLAKE_O );
            }

            ImGui::PushStyleVar( ImGuiStyleVar_ItemSpacing, ImVec2( 3.0f, 3.0f ) );

            if ( ImGui::Button( ICON_FA_REPEAT " Restart" ) )
            {
                cmdExec( "app restart" );
            }

            ImGui::SameLine( );
            if ( ImGui::Button( ICON_KI_EXIT " Exit" ) )
            {
                cmdExec( "exit" );
            }

            ImGui::SameLine( );
            show_stats_ ^= ImGui::Button( ICON_FA_BAR_CHART " Stats" );

            ImGui::PopStyleVar( );
        }

        const bgfx::Stats* stats     = bgfx::getStats( );
        const double       to_ms_cpu = 1000.0 / stats->cpuTimerFreq;
        const double       to_ms_gpu = 1000.0 / stats->gpuTimerFreq;
        const double       frame_ms  = double( stats->cpuTimeFrame ) * to_ms_cpu;

        push_sample( float( frame_ms ) );

        char frame_text_overlay[256];
        bx::snprintf( frame_text_overlay,
                      BX_COUNTOF( frame_text_overlay ),
                      "%s%.3fms, %s%.3fms\nAvg: %.3fms, %.1f FPS",
                      ICON_FA_ARROW_DOWN,
                      min_,
                      ICON_FA_ARROW_UP,
                      max_,
                      avg_,
                      1000.0f / avg_ );

        ImGui::PushStyleColor( ImGuiCol_PlotHistogram,
                               ImColor( 0.0f, 0.5f, 0.15f, 1.0f ).Value );
        ImGui::PlotHistogram( "Frame",
                              &values_[0],
                              static_cast< int >( values_.size( ) ),
                              sample_index_,
                              frame_text_overlay,
                              0.0f,
                              60.0f,
                              ImVec2( 0.0f, 45.0f ) );
        ImGui::PopStyleColor( );

        ImGui::Text( "Submit CPU %0.3f, GPU %0.3f (L: %d)",
                     double( stats->cpuTimeEnd - stats->cpuTimeBegin ) * to_ms_cpu,
                     double( stats->gpuTimeEnd - stats->gpuTimeBegin ) * to_ms_gpu,
                     stats->maxGpuLatency );

        if ( -INT64_MAX != stats->gpuMemoryUsed )
        {
            char tmp0[64];
            bx::prettify( tmp0, BX_COUNTOF( tmp0 ), stats->gpuMemoryUsed );

            char tmp1[64];
            bx::prettify( tmp1, BX_COUNTOF( tmp1 ), stats->gpuMemoryMax );

            ImGui::Text( "GPU mem: %s / %s", tmp0, tmp1 );
        }

        if ( show_stats_ )
        {
            ImGui::SetNextWindowSize( ImVec2( 300.0f, 500.0f ), ImGuiCond_FirstUseEver );

            if ( ImGui::Begin( ICON_FA_BAR_CHART " Stats", &show_stats_ ) )
            {
                if ( ImGui::CollapsingHeader( ICON_FA_PUZZLE_PIECE " Resources" ) )
                {
                    const bgfx::Caps* caps = bgfx::getCaps( );

                    const float item_height = ImGui::GetTextLineHeightWithSpacing( );
                    const float max_width   = 90.0f;

                    ImGui::PushFont( ImGui::Font::Mono );
                    ImGui::Text( "Res: Num  / Max" );
                    resource_bar( "DIB",
                                  "Dynamic index buffers",
                                  stats->numDynamicIndexBuffers,
                                  caps->limits.maxDynamicIndexBuffers,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( "DVB",
                                  "Dynamic vertex buffers",
                                  stats->numDynamicVertexBuffers,
                                  caps->limits.maxDynamicVertexBuffers,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( " FB",
                                  "Frame buffers",
                                  stats->numFrameBuffers,
                                  caps->limits.maxFrameBuffers,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( " IB",
                                  "Index buffers",
                                  stats->numIndexBuffers,
                                  caps->limits.maxIndexBuffers,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( " OQ",
                                  "Occlusion queries",
                                  stats->numOcclusionQueries,
                                  caps->limits.maxOcclusionQueries,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( "  P",
                                  "Programs",
                                  stats->numPrograms,
                                  caps->limits.maxPrograms,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( "  S",
                                  "Shaders",
                                  stats->numShaders,
                                  caps->limits.maxShaders,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( "  T",
                                  "Textures",
                                  stats->numTextures,
                                  caps->limits.maxTextures,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( "  U",
                                  "Uniforms",
                                  stats->numUniforms,
                                  caps->limits.maxUniforms,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( " VB",
                                  "Vertex buffers",
                                  stats->numVertexBuffers,
                                  caps->limits.maxVertexBuffers,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    resource_bar( " VL",
                                  "Vertex layouts",
                                  stats->numVertexLayouts,
                                  caps->limits.maxVertexLayouts,
                                  max_width,
                                  item_height,
                                  resource_color_ );
                    ImGui::PopFont( );
                }

                if ( ImGui::CollapsingHeader( ICON_FA_CLOCK_O " Profiler" ) )
                {
                    if ( 0 == stats->numViews )
                    {
                        ImGui::Text( "Profiler is not enabled." );
                    }
                    else
                    {
                        if ( ImGui::BeginChild( "##view_profiler", ImVec2( 0.0f, 0.0f ) ) )
                        {
                            ImGui::PushFont( ImGui::Font::Mono );

                            ImVec4 cpu_color( 0.5f, 1.0f, 0.5f, 1.0f );
                            ImVec4 gpu_color( 0.5f, 0.5f, 1.0f, 1.0f );

                            const float item_height = ImGui::GetTextLineHeightWithSpacing( );
                            const float item_height_with_spacing =
                                ImGui::GetFrameHeightWithSpacing( );
                            const double to_cpu_ms = 1000.0 / double( stats->cpuTimerFreq );
                            const double to_gpu_ms = 1000.0 / double( stats->gpuTimerFreq );
                            const float  scale     = 3.0f;

                            if ( ImGui::ListBoxHeader(
                                     "Encoders",
                                     ImVec2( ImGui::GetWindowWidth( ),
                                             stats->numEncoders * item_height_with_spacing ) ) )
                            {
                                ImGuiListClipper clipper( stats->numEncoders, item_height );

                                while ( clipper.Step( ) )
                                {
                                    for ( int32_t pos = clipper.DisplayStart;
                                          pos < clipper.DisplayEnd;
                                          ++pos )
                                    {
                                        const bgfx::EncoderStats& encoder_stats =
                                            stats->encoderStats[pos];

                                        ImGui::Text( "%3d", pos );
                                        ImGui::SameLine( 64.0f );

                                        const float max_width = 30.0f * scale;
                                        const float cpu_ms =
                                            float( ( encoder_stats.cpuTimeEnd -
                                                     encoder_stats.cpuTimeBegin ) *
                                                   to_cpu_ms );
                                        const float cpu_width =
                                            bx::clamp( cpu_ms * scale, 1.0f, max_width );

                                        if ( bar( cpu_width,
                                                  max_width,
                                                  item_height,
                                                  cpu_color ) )
                                        {
                                            ImGui::SetTooltip(
                                                "Encoder %d, CPU: %f [ms]", pos, cpu_ms );
                                        }
                                    }
                                }

                                ImGui::ListBoxFooter( );
                            }

                            ImGui::Separator( );

                            if ( ImGui::ListBoxHeader(
                                     "Views",
                                     ImVec2( ImGui::GetWindowWidth( ),
                                             stats->numViews * item_height_with_spacing ) ) )
                            {
                                ImGuiListClipper clipper( stats->numViews, item_height );

                                while ( clipper.Step( ) )
                                {
                                    for ( int32_t pos = clipper.DisplayStart;
                                          pos < clipper.DisplayEnd;
                                          ++pos )
                                    {
                                        const bgfx::ViewStats& view_stats =
                                            stats->viewStats[pos];

                                        ImGui::Text( "%3d %3d %s",
                                                     pos,
                                                     view_stats.view,
                                                     view_stats.name );

                                        const float max_width = 30.0f * scale;
                                        const float cpu_time_elapsed =
                                            float( ( view_stats.cpuTimeEnd -
                                                     view_stats.cpuTimeBegin ) *
                                                   to_cpu_ms );
                                        const float gpu_time_elapsed =
                                            float( ( view_stats.gpuTimeEnd -
                                                     view_stats.gpuTimeBegin ) *
                                                   to_gpu_ms );
                                        const float cpu_width = bx::clamp(
                                            cpu_time_elapsed * scale, 1.0f, max_width );
                                        const float gpuWidth = bx::clamp(
                                            gpu_time_elapsed * scale, 1.0f, max_width );
                                        ImGui::SameLine( 64.0f );

                                        if ( bar( cpu_width,
                                                  max_width,
                                                  item_height,
                                                  cpu_color ) )
                                        {
                                            ImGui::SetTooltip( "View %d \"%s\", CPU: %f [ms]",
                                                               pos,
                                                               view_stats.name,
                                                               cpu_time_elapsed );
                                        }

                                        ImGui::SameLine( );
                                        if ( bar(
                                                 gpuWidth, max_width, item_height, gpu_color ) )
                                        {
                                            ImGui::SetTooltip( "View: %d \"%s\", GPU: %f [ms]",
                                                               pos,
                                                               view_stats.name,
                                                               gpu_time_elapsed );
                                        }
                                    }
                                }
                                ImGui::ListBoxFooter( );
                            }

                            ImGui::PopFont( );
                        }

                        ImGui::EndChild( );
                    }
                }
            }
            ImGui::End( );
        }
    }
}

void
    dialog::triangulation_header( )
{
    auto       current        = generator_selection::invalid;
    auto const prev_debug_box = debug_box_;
    if ( ImGui::CollapsingHeader( "Triangulation" ) )
    {
        ImGui::Checkbox( "Square bounds", &debug_box_ );
        ImGui::Checkbox( "Render bounds", &render_bounds_ );

        if ( ImGui::BeginTabBar( "Generators", ImGuiTabBarFlags_None ) )
        {
            ImGui::Indent( );
            if ( ImGui::BeginTabItem( "Jitter" ) )
            {
                current = generator_selection::jitter;
                ImGui::SliderInt( "Spacing", &points_grid_count_, 2, 32 );
                ImGui::SliderFloat( "Pass ratio", &generator_pass_rate_, 0.1f, 0.9999f );
                ImGui::Checkbox( "Reference points", &render_ref_points_ );

                ImGui::EndTabItem( );
            }

            if ( ImGui::BeginTabItem( "Hex" ) )
            {
                current            = generator_selection::hex;
                render_ref_points_ = false;
                ImGui::SliderInt( "Spacing", &points_grid_count_, 2, 32 );

                ImGui::EndTabItem( );
            }

            if ( ImGui::BeginTabItem( "Fib spiral" ) )
            {
                current            = generator_selection::fib;
                render_ref_points_ = false;
                ImGui::SliderInt( "Points", &fib_points_count_, 8, 512 );
                ImGui::SliderFloat( "Scale", &fib_scale_, 8, 64 );

                ImGui::EndTabItem( );
            }

            ImGui::Unindent( );
            ImGui::EndTabBar( );
        }

        if ( ImGui::Button( "Generate points" ) )
        {
            if ( current == generator_selection::jitter )
            {
                generate_jitter_points_(
                    prev_debug_box, points_grid_count_, generator_pass_rate_ );
            }
            else if ( current == generator_selection::hex )
            {
                generate_hex_points_( prev_debug_box, points_grid_count_ );
            }
            else if ( current == generator_selection::fib )
            {
                generate_fib_points_( prev_debug_box, fib_points_count_, fib_scale_ );
            }
            else
            {
                assert( false );
            }
        }

        ImGui::SameLine( );
        if ( ImGui::Button( "Process points" ) )
        {
            triangulate_points_( );
        }

        if ( ImGui::Button( "Generate and process points" ) )
        {
            if ( current == generator_selection::jitter )
            {
                generate_jitter_points_(
                    prev_debug_box, points_grid_count_, generator_pass_rate_ );
            }
            else if ( current == generator_selection::hex )
            {
                generate_hex_points_( prev_debug_box, points_grid_count_ );
            }
            else if ( current == generator_selection::fib )
            {
                generate_fib_points_( prev_debug_box, fib_points_count_, fib_scale_ );
            }
            else
            {
                assert( false );
            }
            triangulate_points_( );
        }

        if ( ImGui::BeginTabBar( "Render options", ImGuiTabBarFlags_None ) )
        {
            ImGui::Indent( );
            if ( ImGui::BeginTabItem( "Delaunay" ) )
            {
                ImGui::Checkbox( "Render seeds", &render_points_ );
                if ( render_points_ )
                {
                    ImGui::ColorEdit3( "Points", points_colour_.data( ) );
                }

                ImGui::Checkbox( "Render triangles", &render_delaunay_ );
                if ( render_delaunay_ )
                {
                    ImGui::ColorEdit3( "TriEdge", triangle_colour_.data( ) );
                }

                ImGui::Checkbox( "Render circumcircles", &render_circumcircles_ );
                if ( render_circumcircles_ )
                {
                    ImGui::ColorEdit3( "CirleEdge", circumcircle_colour_.data( ) );
                }

                ImGui::EndTabItem( );
            }

            if ( ImGui::BeginTabItem( "Voronoi" ) )
            {
                ImGui::Checkbox( "Render regions", &render_regions_ );
                if ( render_regions_ )
                {
                    ImGui::ColorEdit3( "RegEdge", region_colour_.data( ) );
                    ImGui::ColorEdit3( "ClipRegEdge", clipped_region_colour_.data( ) );
                }

                ImGui::Checkbox( "Render region fill", &render_region_fill_ );
                if ( render_region_fill_ )
                {
                    ImGui::ColorEdit3( "FillColour", region_fill_colour_.data( ) );
                }

                ImGui::Checkbox( "Render centroid", &render_centroid_ );
                if ( render_centroid_ )
                {
                    ImGui::ColorEdit3( "CentroidPoint", centroid_colour_.data( ) );
                }

                ImGui::EndTabItem( );
            }

            ImGui::Unindent( );
            ImGui::EndTabBar( );
        }
    }
}
