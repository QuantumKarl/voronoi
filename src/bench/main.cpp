#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include "fixture.hpp"
#include <catch.hpp>
#include <sys_generic.hpp>

static constexpr size_t start_index    = 4;
static constexpr size_t number_samples = 9;

void
    jitter_triangulate( size_t i_size )
{
    std::stringstream s;
    fixture           data( algorithm::generate::jitter_sample );
    data.init( i_size );

    s << "jitter_sampling:{size:" << i_size << ",count:" << data.get_count( ) << '}';
    BENCHMARK( s.str( ).c_str( ) )
    {
        data.triangulate( );
    };
}

TEST_CASE( "jitter-triangulate", "[benchmark]" )
{
    size_t constexpr start = start_index;            // from 2^start
    size_t constexpr end   = start + number_samples; // to 2^end
    for ( size_t i = start; i <= end; ++i )
    {
        jitter_triangulate( i );
    }
}

void
    hex_triangulate( size_t i_size )
{
    auto generator = []( auto const  i_subdivision,
                         auto const  i_pass_rate,
                         auto const& i_area,
                         auto&       io_generator,
                         auto&       io_random_points,
                         auto&       io_ref_points,
                         auto&       io_halfwidths ) {
        unused( i_pass_rate, io_generator, io_ref_points, io_halfwidths );
        algorithm::generate::hex_grid( i_subdivision, i_area, io_random_points );
    };

    fixture data( generator );
    data.init( i_size );
    std::stringstream s;

    s << "hex_grid:{size:" << i_size << ",count:" << data.get_count( ) << '}';
    BENCHMARK( s.str( ).c_str( ) )
    {
        data.triangulate( );
    };
}

TEST_CASE( "hex-triangulate", "[benchmark]" )
{
    size_t constexpr start = start_index;            // from 2^start
    size_t constexpr end   = start + number_samples; // to 2^end
    for ( size_t i = start; i <= end; ++i )
    {
        hex_triangulate( i );
    }
}
