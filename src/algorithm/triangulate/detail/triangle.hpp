#if !defined( D_TRIANGLE_HPP )
#define D_TRIANGLE_HPP

#include "../../geometry/shapes.hpp"
#include "hashes.hpp"
#include "pointer_edge.hpp"
#include "value_edge.hpp"

#include <list>
#include <sys_hash.hpp>
#include <sys_trie.hpp>
#include <unordered_set>
#include <vector>

namespace algorithm::triangluate
{
class delaunay;
}

namespace algorithm::triangluate::detail
{
using namespace sys;
using namespace tsl;

class region;
class circumcircle;

using polygon_edges_cont = std::vector< pointer_edge >;
using circle_cont_t      = std::list< circumcircle >;
using circle_itr         = circle_cont_t::iterator;
using circle_itr_cont_t  = std::vector< circle_itr >;
using circle_itr_set_t   = std::unordered_set< circle_itr, deref_addressof_hash >;

// TODO: maybe a list of points
class triangle
{
    using pointer_edges_t = std::array< pointer_edge, 3 >;
    using value_edges_t   = std::array< value_edge, 3 >;

  public:
    // TODO: change to iterators
    using point_ptrs_t = std::array< vec2 const*, 3 >; // points to original data

    triangle( vec2 const& i_a,
              vec2 const& i_b,
              vec2 const& i_c,
              circle_itr  i_new_circle ) noexcept;

    vec2 const& a( ) const;
    vec2 const& b( ) const;
    vec2 const& c( ) const;

    geometry::circle const& circumcircle( ) const;

    // TODO: const_itr version for public interface
    circle_itr circumcircle_itr( ) const;

    friend class ::algorithm::triangluate::delaunay;

  private:
    bool collide_circumcircle( vec2 const& i_point ) const;

    void set_bad( );
    bool get_bad( ) const;

    pointer_edges_t create_pointer_edges( ) const;
    value_edges_t   create_value_edges( ) const;

    point_ptrs_t points_;
    circle_itr   circumcircle_;
    bool         bad_;
};

//--------------------------------
// triangle trie containers
using triangle_cont_t     = std::list< triangle >;
using triangle_itr        = triangle_cont_t::iterator;
using triangle_itr_cont_t = std::vector< triangle_itr >; // used for function arguments

using tri_edge_cont_t      = std::list< std::array< triangle_itr, 2 > >;
using tri_point_map_value  = tri_edge_cont_t::iterator;
using triangle_point_map_t = htrie_map< char, tri_point_map_value, xxhash_data, unsigned char >;

using tri_itr_set_t = std::unordered_set< triangle_itr, deref_addressof_hash >;

} // namespace algorithm::triangluate::detail

#endif
