#if !defined( D_POINTER_EDGE_HPP )
#define D_POINTER_EDGE_HPP

#include <array>
#include <sys_eigen.hpp>

namespace algorithm::triangluate::detail
{
using namespace sys;

class pointer_edge final
{
  public:
    using points_t = std::array< vec2 const*, 2 >;

    pointer_edge( vec2 const& i_a, vec2 const& i_b ) noexcept;

    vec2 const& a( ) const;
    vec2 const& b( ) const;

  private:
    points_t points_;
};

} // namespace algorithm::triangluate::detail

#endif
