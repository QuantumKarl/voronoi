#if !defined( D_HEX_GRID_HPP )
#define D_HEX_GRID_HPP
#include "../geometry/shapes.hpp"

#include <sys_eigen.hpp>

namespace algorithm::generate
{
using namespace sys;

void hex_grid( size_t const            i_subdivision,
               geometry::box const&    i_area,
               aligned_vector< vec2 >& io_random_points );

} // namespace algorithm::generate

#endif
