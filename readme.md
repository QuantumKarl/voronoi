# Delaunay triangulation and Voronoi diagram algorithm

This project implements the Delaunay triangulation algorithm and Voronoi diagram (graph) algorithm in a combined algorithm (see references for details).
What that means is:

* the user provides a set of points in 2D space and an axis aligned bounding box which all the points are contained within
* the algorithm creates a triangulation of all the points
* the algorithm then uses that triangulation to create a Voronoi diagram
* the triangulation and voronoi data are returned to the user in an object which can be queried and iterated over
* finally the user discards the object after extracting the data that is interesting to their application

The objective of this project is to provide a useful and easy to consume result from these algorithms.
In other projects that I have looked at, they only create the triangulation **or** Voronoi diagram.
Moreover, the triangulation and Voronoi diagrams are interconnected structures, one comes from the other (they are each-other's dual graph).
I believe this means that the result of the algorithm should allow the user to traverse the data in the same way.

With this library the user can:

* Iterate all triangles
	* From that triangle get its circumcircle 
		* From that circumcircle get all the Voronoi regions that use it as a vertex (connected Voronoi regions)
	* From that triangle get each of its points
		* From that point get each triangle which uses that point (adjacent triangles)
* Iterate all circumcircles
	* From that circumcircle get all the Voronoi regions that use it as a vertex (connected Voronoi regions)
* Iterate all regions
	* From that region get all triangles (connected Delaunay triangles)
	* From that region get all its points
		* From that point get all Voronoi regions which also use it as a vertex (adjacent regions)

## Getting started

This project is intended to be used as a [submodule](https://git-scm.com/docs/git-submodule). Include this repo as a git module and add `include` directory to your compilation. Then `#include <delaunay.hpp>`.
See the unit tests in `src/test/main.cpp`for an example on how to use the library.

This project includes a graphical demo which generates points randomly for input and renders the result.
The image below is graphical demo rendering the same triangulation that Wikipedia uses as its [example](https://en.wikipedia.org/wiki/File:Delaunay_Voronoi.svg):



The user can select different types of generation functions for the points 


## Dependencies

* C++17 compiler (core algorithm)
* [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) (core algorithm)
* [HAT-Trie](https://github.com/Tessil/hat-trie) (core algorithm)
* [Catch2](https://github.com/catchorg/Catch2) (unit tests)
* [BGFX](https://github.com/bkaradzic/bgfx) (visual demo)

## Building
To build the demo and tests you can:

* build via the provided visual studio solution file
* build via [meson](https://mesonbuild.com/index.html) build file
* build via docker file

## Verification
The unit tests are verified with:

* [Dr. Memory](https://drmemory.org) on Windows
* [Valgrind](https://valgrind.org)(memcheck) on Linux

No errors or warnings are reported on Linux with Valgrind (with AVX2 enabled).  
On Windows Dr. Memory reports errors only when AVX2 is enabled, this leads me to conclude that either the Windows compiler is generating incorrect code or Dr. Memory is reporting false positives (most likely).

The codebase is statically analysed with:

* [PVS studio](https://www.viva64.com/en/pvs-studio/)
* [CppCheck](http://cppcheck.sourceforge.net)
* MSVC and Clang compiler warnings

No errors or warnings reported.

## Performance
This project uses the Bowyer-Watson algorithm. This algorithm was selected due to its simplicity and ability to work in higher dimensions. After further research it was apparent that Bowyer-Watson is also a good choice for implementing concurrency in Delaunay triangulation, making it a flexible solution with potential.
However, without optimisations such as Biased Random Insertion Order (BRIO) Bowyer-Watson doesn't perform well compared to other 2D only algorithms.

## Possible future enhancements

### Features:

* Loyd's algorithm
* Lookup of region via mouse click (via Quad / Oct tree)
* Compile using [emscripten](https://emscripten.org) for [wasm](https://webassembly.org) or [asm.js](http://asmjs.org)
* Support for arbitrary dimensional data
* Support for Voronoi region clipping against more shapes (currently only square is supported)
* Create a simple territory based game
### Optimisations:

* Quad / Oct tree to accelerate triangle lookup or
* BRIO and triangle walking
* Improve triangle clipping algorithm
* Fork and join based concurrency
	* Load balancing via spatial partitioning
* Change usage of HAT-Trie to be based on [half edges](https://doc.cgal.org/latest/HalfedgeDS/index.html) (this might reduce query performance, but removes a dependency)
### Minor todo:

* More documentation
* cmake or scons builds

## References

* [Delaunay triangulation](https://en.wikipedia.org/wiki/Delaunay_triangulation)
* [Voronoi diagram](https://en.wikipedia.org/wiki/Voronoi_diagram)
* [Lloyd's algorithm](https://en.wikipedia.org/wiki/Lloyd%27s_algorithm)
* [Bowyer-Watson algorithm](https://en.wikipedia.org/wiki/Bowyer%E2%80%93Watson_algorithm)
* [How do I derive a Voronoi diagram given its point set and its Delaunay triangulation?](https://stackoverflow.com/questions/85275/how-do-i-derive-a-voronoi-diagram-given-its-point-set-and-its-delaunay-triangula)
* [GEODESIC CENTROIDAL VORONOI TESSELLATIONS: THEORIES, ALGORITHMS AND APPLICATIONS](https://arxiv.org/pdf/1907.00523.pdf)
* [PatCC1: an efficient parallel triangulation algorithm for spherical and planar grids with commonality and parallel consistency](https://www.geosci-model-dev.net/12/3311/2019/)
* [Triangulations in CGAL](https://hal.inria.fr/inria-00167199/document)
* [Load-Balancing for Parallel Delaunay Triangulations](https://arxiv.org/pdf/1902.07554.pdf)
* [CPU Performance Evaluation for 2D Voronoi Tessellation](http://www.diva-portal.org/smash/get/diva2:1333680/FULLTEXT02)
* [One machine, one minute, three billion tetrahedra](https://arxiv.org/pdf/1805.08831.pdf)
* [An Introduction to Automatic Mesh Generation Algorithms* Part I](https://www.osti.gov/biblio/1394106)
* [Fast Segment Insertion and Incremental Construction of Constrained Delaunay Triangulations](https://people.eecs.berkeley.edu/~jrs/papers/inccdt.pdf)
* [Quality-Mesh Generation and Reconstruction for Engineering Design Optimisation](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjqstap64nqAhWJYsAKHbAlBWwQFjAAegQIAxAB&url=https://estudogeral.sib.uc.pt/bitstream/10316/35689/1/Quality-Mesh%2520Generation%2520and%2520Reconstruction%2520for%2520Engineering%2520Design%2520Optimisation.pdf&usg=AOvVaw1tmwaZkyf1fiABbriXkvXd)
* [Lecture Notes on Delaunay Mesh Generation](https://people.eecs.berkeley.edu/~jrs/meshpapers/delnotes.pdf)
* [Incremental Constructions con BRIO](https://people.eecs.berkeley.edu/~jrs/meshpapers/BRIO.pdf)
* [A survey of parallel mesh generation methods](https://www.brown.edu/research/projects/scientific-computing/sites/brown.edu.research.projects.scientific-computing/files/uploads/A%20Survey%20of%20Parallel%20Mesh%20Generation%20Methods.pdf)
* [Task Parallel Implementation of the Bowyer-Watson Algorithm](https://www.researchgate.net/publication/2607476_Task_Parallel_Implementation_of_the_Bowyer-Watson_Algorithm)

## MIT License

Copyright (c) 2020 [Karl Hutchinson](http://www.quantumkarl.co.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
