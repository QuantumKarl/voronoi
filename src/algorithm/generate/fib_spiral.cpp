#include "../geometry/comparison.hpp"
#include "fib_spiral.hpp"

namespace algorithm::generate
{
//--------------------------------
/*
    Vogel's model (fermat spiral)
*/
void
    fib_spiral( size_t const            i_count,
                real_t const            i_scale,
                geometry::box const&    i_area,
                aligned_vector< vec2 >& io_points )
{
    io_points.clear( );
    vec2 const center = ( i_area.min + i_area.max ) / 2.0;

    for ( size_t i = 1; i < i_count; ++i )
    {
        real_t const n = real_t( i );
        // polar coords
        real_t const r = i_scale * std::sqrt( n );
        real_t const t = n * 2.39996322972865332; // golden angle
        // polar to cartesian
        real_t const x = r * std::cos( t );
        real_t const y = r * std::sin( t );
        vec2         p( x, y );

        // translate
        p += center;

        // filter
        if ( box_point_collision( i_area, p ) )
        {
            io_points.emplace_back( std::move( p ) );
        }
    }

    // sort points
    auto compare = []( vec2 const& p1, vec2 const& p2 ) {
        if ( geometry::nearly_equal( p1.x( ), p2.x( ) ) == false )
        {
            return p1.x( ) < p2.x( );
        }
        else
        {
            return p1.y( ) < p2.y( );
        }
    };

    std::sort( std::begin( io_points ), std::end( io_points ), compare );
}

} // namespace algorithm::generate
