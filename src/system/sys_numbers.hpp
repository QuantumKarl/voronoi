#if !defined( D_SYS_NUMBERS_HPP )
#define D_SYS_NUMBERS_HPP
#include "sys_types.hpp"

namespace sys
{
// 20 digits of pi
static constexpr real_t pi  = static_cast< real_t >( 3.14159265358979323846l );
static constexpr real_t pi2 = static_cast< real_t >( pi * 2.0l );

static constexpr real_t
    deg_to_rad( real_t const i_deg )
{
    return i_deg * ( pi / 180.0 );
}

} // end of namespace sys
#endif
