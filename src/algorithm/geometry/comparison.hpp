#if !defined( D_COMPARISON_HPP )
#define D_COMPARISON_HPP
#include <sys_eigen.hpp>

namespace algorithm::geometry
{
using namespace sys;

//--------------------------------
/* comparison with epsilon and relative error
https://floating-point-gui.de/errors/comparison/
*/
template < typename real_type >
bool
    nearly_equal( real_type a, real_type b )
{
    real_type constexpr min_normal = std::numeric_limits< real_type >::denorm_min( );
    real_type constexpr max        = std::numeric_limits< real_type >::max( );
    real_type constexpr epsilon    = std::numeric_limits< real_type >::epsilon( );

    real_type const abs_a = std::abs( a );
    real_type const abs_b = std::abs( b );
    real_type const diff  = std::abs( a - b );

    if ( a == b )
    {
        // shortcut, handles infinities
        return true;
    }
    else if ( a == 0 || b == 0 || ( abs_a + abs_b < min_normal ) )
    {
        // a or b is zero or both are extremely close to it
        // relative error is less meaningful here
        return diff < ( epsilon * min_normal );
    }
    else
    {
        // use relative error
        return diff / std::min( ( abs_a + abs_b ), max ) < epsilon;
    }
}

//--------------------------------
/* counter clock wise less than
	aka sorts two points relative to another point (which is called "center"
	in this function)
	https://stackoverflow.com/questions/6989100/sort-points-in-clockwise-order
*/
bool less( vec2 const& center, vec2 const& a, vec2 const& b );

} // namespace algorithm::geometry
#endif
