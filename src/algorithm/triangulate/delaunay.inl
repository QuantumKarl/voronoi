
template < typename value_type, typename... args_t >
auto
    delaunay::emplace_back( std::list< value_type >& io_list, args_t&&... i_values )
{
    return io_list.emplace( std::cend( io_list ), std::forward< args_t >( i_values )... );
}

//--------------------------------
template < typename point_map_t, typename data_cont_t, typename edges_t, typename itr_t >
void
    delaunay::set_edge_in_point_map( data_cont_t&        i_data,
                                     point_map_t&        io_point_map,
                                     edges_t&            io_edges,
                                     itr_t               i_itr,
                                     detail::value_edge& i_edge )
{
    assert( i_edge.size( ) >= detail::value_edge::size_one( ) );
    auto end = std::end( i_data );

    auto result =
        io_point_map.insert_ks( i_edge.get_ptr( ), i_edge.size( ), std::end( io_edges ) );
    // if it wasn't added, therefore there must be a value present
    if ( result.second == false )
    {
        auto& value = **result.first;

        assert( *result.first != std::end( io_edges ) );
        assert( value[0] != i_itr );
        assert( value[1] == end );

        // find the first value that is set to end
        // and then set it to self
        for ( auto& data_itr : value )
        {
            assert( data_itr != i_itr );
            if ( data_itr == end )
            {
                data_itr = i_itr;
                break;
            }
        }

        assert( ( value[0] == end && value[1] == end ) || ( value[0] != value[1] ) );
    }
    else // it was just inserted so create a value in edges
    {
        auto new_itr = emplace_back( io_edges, typename edges_t::value_type( { i_itr, end } ) );
        ( *result.first ) = new_itr;

        // insert reverse key
        if ( i_edge.size( ) == detail::value_edge::size_two( ) )
        {
            i_edge.reset( i_edge.b( ), i_edge.a( ) );
            io_point_map.insert_ks( i_edge.get_ptr( ), i_edge.size( ), new_itr );
        }
    }
    // check_memory_error( );
}
