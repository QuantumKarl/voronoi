#if !defined( D_FIB_SPIRAL_HPP )
#define D_FIB_SPIRAL_HPP
#include "../geometry/shapes.hpp"

#include <sys_eigen.hpp>

namespace algorithm::generate
{
using namespace sys;

void fib_spiral( size_t const            i_count,
                 real_t const            i_scale,
                 geometry::box const&    i_area,
                 aligned_vector< vec2 >& io_points );

} // namespace algorithm::generate
#endif
