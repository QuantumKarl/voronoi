#include "imgui/imgui.h"
#include <array>
#include <entry/entry.h>
#include <functional>

class dialog
{
    enum class generator_selection : size_t
    {
        invalid = 0,
        jitter,
        hex,
        fib
    };

  public:
    using colour3_t = std::array< float, 3 >;

    dialog( ) noexcept;

    template < typename func_gen1_t,
               typename func_gen2_t,
               typename func_gen3_t,
               typename triangulate_func_t >
    void
        init( entry::AppI*       io_app,
              func_gen1_t        io_generate_jitter_points,
              func_gen2_t        io_generate_hex_points,
              func_gen3_t        io_generate_fib_points,
              triangulate_func_t io_triangulate )
    {
        assert( app_ == nullptr );
        app_                    = io_app;
        generate_jitter_points_ = io_generate_jitter_points;
        generate_hex_points_    = io_generate_hex_points;
        generate_fib_points_    = io_generate_fib_points;
        triangulate_points_     = io_triangulate;
    }

    void draw_dialog( const char* i_error_text );

    bool             get_debug_box( ) const;
    bool             get_clear_points( );
    bool             get_render_points( ) const;
    colour3_t const& get_points_colour( ) const;
    bool             get_render_ref_points( ) const;
    bool             get_render_circumcircles( ) const;
    colour3_t const& get_circumcircle_colour( ) const;
    bool             get_render_bounds( ) const;
    bool             get_render_delaunay( ) const;
    colour3_t const& get_delaunay_colour( ) const;
    bool             get_render_regions( ) const;
    colour3_t const& get_region_colour( ) const;
    colour3_t const& get_clipped_region_colour( ) const;
    bool             get_render_centroid( ) const;
    colour3_t const& get_centroid_colour( ) const;
    bool             get_render_region_fill( ) const;
    colour3_t const& get_region_fill_colour( ) const;

  private:
    void program_header( const char* i_error_text );
    void triangulation_header( );

    void push_sample( float value );

    static bool bar( float i_width, float i_max_width, float i_height, const ImVec4& i_color );
    static void resource_bar( const char*   i_name,
                              const char*   i_tooltip,
                              uint32_t      i_num,
                              uint32_t      i_max,
                              float         i_max_width,
                              float         i_height,
                              const ImVec4& i_resource_color );

    std::array< float, 100 >                  values_;
    std::function< void( bool, int, float ) > generate_jitter_points_;
    std::function< void( bool, int ) >        generate_hex_points_;
    std::function< void( bool, int, float ) > generate_fib_points_;
    std::function< void( void ) >             triangulate_points_;
    ImVec4                                    resource_color_;
    colour3_t                                 points_colour_;
    colour3_t                                 triangle_colour_;
    colour3_t                                 circumcircle_colour_;
    colour3_t                                 region_colour_;
    colour3_t                                 clipped_region_colour_;
    colour3_t                                 centroid_colour_;
    colour3_t                                 region_fill_colour_;
    entry::AppI*                              app_;
    int32_t                                   sample_index_;
    float                                     min_;
    float                                     max_;
    float                                     avg_;
    int                                       points_grid_count_;
    float                                     generator_pass_rate_;
    int                                       fib_points_count_;
    float                                     fib_scale_;
    bool                                      debug_box_;
    bool                                      render_points_;
    bool                                      render_ref_points_;
    bool                                      render_circumcircles_;
    bool                                      render_bounds_;
    bool                                      render_delaunay_;
    bool                                      render_regions_;
    bool                                      render_centroid_;
    bool                                      render_region_fill_;
    bool                                      clear_points_;
    bool                                      show_stats_;
};
