# Source folder

This folder contains all the source code.
Sub-folders from the outer most level, I.E. src/demo, is an executable or a library.
Sub-folders in executable or library folders are encapsulated in namespaces.