CC=clang CXX=clang++ LD=ld.lld meson clang-debug
CC=clang CXX=clang++ LD=ld.lld meson clang-release
CC=clang CXX=clang++ LD=ld.lld meson clang-san

cd clang-debug
meson configure -Dbuildtype=debug -Dcpp_link_args=-fuse-ld=lld
cd ..

cd clang-release 
meson configure -Dbuildtype=release -Db_ndebug=true -Dcpp_link_args=-fuse-ld=lld
cd ..

cd clang-san
meson configure -Dbuildtype=debugoptimized -Db_sanitize=undefined -Dcpp_link_args=-fuse-ld=lld
cd ..