#include "collision_region.hpp"

namespace algorithm::triangluate::detail
{
//--------------------------------
collision_region::collision_region( ) noexcept
    : triangles_( )
    , min_x_( std::numeric_limits< real_t >::min( ) )
    , max_x_( std::numeric_limits< real_t >::max( ) )
{
}

//--------------------------------
collision_region::collision_region( real_t const i_min_x, real_t const i_max_x ) noexcept
    : triangles_( )
    , min_x_( i_min_x )
    , max_x_( i_max_x )
{
}

//--------------------------------
bool
    collision_region::collide_with_triangle( triangle_itr i_itr ) const
{
    auto const& tri    = *i_itr;
    auto const& circle = tri.circumcircle( );
    real_t      max_x  = circle.p.x( ) + circle.r;
    return max_x >= min_x_;
}

//--------------------------------
void
    collision_region::add_triangle( triangle_itr i_itr )
{
    triangles_.emplace_back( i_itr );
}

//--------------------------------
void
    collision_region::remove_triangle( triangle_itr i_itr )
{
    for ( auto itr = std::begin( triangles_ ), end = std::end( triangles_ ); itr != end; ++itr )
    {
        if ( *itr == i_itr )
        {
            triangles_.erase( itr );
            return;
        }
    }
}

//--------------------------------
collision_region&
    collision_region::operator+=( collision_region const& i_other )
{
    for ( auto itr = std::begin( i_other.triangles_ ), end = std::end( i_other.triangles_ );
          itr != end;
          ++itr )
    {
        if ( collide_with_triangle( *itr ) )
        {
            add_triangle( *itr );
        }
    }
    return *this;
}

//--------------------------------
void
    collision_region::clear_triangles( )
{
    triangles_.clear( );
}

//--------------------------------
triangle_itr_cont_t const&
    collision_region::get_triangles( )
{
    return triangles_;
}

//--------------------------------
real_t&
    collision_region::get_min_x( )
{
    return min_x_;
}

//--------------------------------
real_t&
    collision_region::get_max_x( )
{
    return max_x_;
}

} // namespace algorithm::triangluate::detail
