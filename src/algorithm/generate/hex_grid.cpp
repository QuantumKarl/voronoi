#include "hex_grid.hpp"
#include "../geometry/comparison.hpp"

namespace algorithm::generate
{
void
    hex_grid( size_t const            i_subdivision,
              geometry::box const&    i_area,
              aligned_vector< vec2 >& io_random_points )
{
    assert( i_subdivision > 0 );
    assert( i_area.min.x( ) < i_area.max.x( ) && i_area.min.y( ) < i_area.max.y( ) );

    io_random_points.clear( );

    vec2 const   dimensions = i_area.max - i_area.min;
    real_t const width      = dimensions.x( ) / static_cast< real_t >( i_subdivision );
    real_t const height     = dimensions.y( ) / static_cast< real_t >( i_subdivision );

    real_t const halfwidth  = width / 2.0;
    real_t const halfheight = height / 2.0;

    real_t const hex_width  = std::sqrt( 3.0 ) * halfwidth;
    real_t const hex_height = 2.0 * halfheight;

    real_t const x_offset = hex_width;
    real_t const y_offset = hex_height * ( 3.0 / 4.0 );

    size_t index = 2;
    for ( real_t y = i_area.min.y( ) + ( y_offset / 2.0 ), y_max = i_area.max.y( ); y <= y_max;
          y += y_offset )
    {
        real_t const offset = index++ % 2 == 0 ? ( x_offset / 2.0 ) : ( x_offset );
        for ( real_t x = i_area.min.x( ) + offset, x_max = i_area.max.x( ); x <= x_max;
              x += x_offset )
        {
            io_random_points.emplace_back( x, y );
        }
    }

    auto compare = []( vec2 const& p1, vec2 const& p2 ) {
        if ( geometry::nearly_equal( p1.x( ), p2.x( ) ) == false )
        {
            return p1.x( ) < p2.x( );
        }
        else
        {
            return p1.y( ) < p2.y( );
        }
    };

    std::sort( std::begin( io_random_points ), std::end( io_random_points ), compare );
}

} // namespace algorithm::generate
