#if !defined( D_PLATFORM_HPP )
#define D_PLATFORM_HPP
#include <cstddef>

namespace platform
{
//----------------------------------------------------------------
// detect instruction_sets
// http://nadeausoftware.com/articles/2012/02/c_c_tip_how_detect_processor_type_using_compiler_predefined_macros

enum class instruction_sets
{
    x64,
    x32
};

#if defined( __x86_64__ ) || defined( _M_X64 )
// x86 64-bit
constexpr instruction_sets  instruction_set     = instruction_sets::x64;
constexpr char const* const instruction_set_str = "x64";
#elif defined( __i386 ) || defined( _M_IX86 )
// x86 32-bit
constexpr instruction_sets  instruction_set     = instruction_sets::x32;
constexpr char const* const instruction_set_str = "x32";
#else
static_assert( false, "ERROR: unknown instruction set" );
#endif

//----------------------------------------------------------------
// detect compiler
// http://nadeausoftware.com/articles/2012/10/c_c_tip_how_detect_compiler_name_and_version_using_compiler_predefined_macros

enum class compilers
{
    llvm,
    gcc,
    msvc
};

#if defined( __clang__ )
// Clang/LLVM
constexpr compilers         compiler               = compilers::llvm;
constexpr char const* const compiler_full_version  = __VERSION__;
constexpr size_t            compiler_major_version = static_cast< size_t >( __clang_major__ );
constexpr size_t            compiler_minor_version = static_cast< size_t >( __clang_minor__ );
constexpr char const* const compiler_str           = "LLVM";
#define CLANG 1
#define COMPILER CLANG
#elif defined( __ICC ) || defined( __INTEL_COMPILER )
// Intel ICC/ICPC
static_assert( false, "ERROR: unsupported compilers Intel ICC/ICPC" );
#elif defined( __GNUC__ ) || defined( __GNUG__ )
// GNU GCC/G++
constexpr compilers         compiler               = compilers::gcc;
constexpr char const* const compiler_full_version  = __VERSION__;
constexpr size_t            compiler_major_version = static_cast< size_t >( __GNUC__ );
constexpr size_t            compiler_minor_version = static_cast< size_t >( __GNUC_MINOR__ );
constexpr char const* const compiler_str           = "gcc";
#define GCC 2
#define COMPILER GCC
#elif defined( __HP_cc ) || defined( __HP_aCC )
// Hewlett-Packard C/aC++
static_assert( false, "ERROR: unsupported compilers Hewlett-Packard C/aC++" );
#elif defined( __IBMC__ ) || defined( __IBMCPP__ )
// IBM XL C/C++
static_assert( false, "ERROR: unsupported compilers IBM XL C/C++" );
#elif defined( _MSC_VER )
// Microsoft Visual Studio
constexpr compilers compiler = compilers::msvc;
// NOTE: _MSC_FULL_VER added in msvc 2008
// NOTE: _MSC_FULL_VER is a number instead of a string like the others
constexpr size_t            compiler_full_version  = _MSC_FULL_VER;
constexpr size_t            compiler_major_version = static_cast< size_t >( _MSC_VER );
constexpr size_t            compiler_minor_version = static_cast< size_t >( _MSC_VER );
constexpr char const* const compiler_str           = "msvc";
#define MSC 3
#define COMPILER MSC
#elif defined( __PGI )
// Portland Group PGCC/PGCPP
static_assert( false, "ERROR: unsupported compilers Portland Group PGCC/PGCPP" );
#elif defined( __SUNPRO_C ) || defined( __SUNPRO_CC )
// Oracle Solaris Studio
static_assert( false, "ERROR: unsupported compilers Orcale Solaris Studio" );
#else
static_assert( false, "ERROR: unknown compilers" );
#endif

//----------------------------------------------------------------
// detect operating system
// http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system

enum class operating_systems
{
    linux,
    cygwin,
    windows,
};

#if defined( __linux__ )
// Linux
constexpr operating_systems operating_system     = operating_systems::linux;
constexpr char const* const operating_system_str = "Linux";
#elif defined( __CYGWIN__ ) && !defined( _WIN32 )
// Cygwin POSIX under Microsoft Windows
constexpr operating_systems operating_system     = operating_systems::cygwin;
constexpr char const* const operating_system_str = "Cygwin";
#elif defined( _WIN64 ) || defined( _WIN32 )
// Microsoft Windows
constexpr operating_systems operating_system     = operating_systems::windows;
constexpr char const* const operating_system_str = "Windows";
#else
static_assert( false, "ERROR: unknown operating system" );
#endif
} // namespace platform

#if defined( COMPILER ) && ( COMPILER == MSC )
#define DISABLE_WARNINGS __pragma( warning( push, 0 ) )
#define ENABLE_WARNINGS __pragma( warning( pop ) )
#elif defined( COMPILER ) && ( COMPILER == GCC || COMPILER == CLANG )
// clang-format off
#define DO_PRAGMA(X) _Pragma(#X)

#define DISABLE_WARNING(warningName) \
DO_PRAGMA(GCC diagnostic ignored #warningName)

#define DISABLE_WARNINGS DO_PRAGMA(GCC diagnostic push) \
DISABLE_WARNING(-Weverything)

#define ENABLE_WARNINGS DO_PRAGMA( GCC diagnostic pop )
// clang-format on
#else
#error "ERROR: unkown platform"
#endif

#endif
