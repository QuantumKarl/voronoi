# -*- coding: utf-8 -*-

#---------------------------- linkers flags ----------------------------
# This python script is called from the meson build script.
# It is given bools to indicate the current: compiler, os, isa etc.
# With these bools it then creates command line flags for the linker.
# Some flags are intended to be toggled. These can be changed via changing
# a variable in the script, see below starting at the "general" comment.

import sys
from enum import Enum, unique
from command_args import args_to_flags, arg_keys

def msvc_args(args):
    flags = []
    
    #---- general ----
    flags.append("NOLOGO") # suppress startup banner
    flags.append("WX") # warnings as errors
    
    #---- input ----
    
    #---- manifest ----
    flags.append("MANIFEST")
    flags.append("ALLOWISOLATION")
    flags.append("MANIFESTUAC:NO")
    
    #---- debugging ----
    
    #---- system ----
    # dont set the subsystem
    # can set heap sizes etc here
    
    #---- optimisation ----
    if not args.has_opt and args.has_debug:
        pass
    else:
        flags.append("OPT:REF") # references
        flags.append("OPT:ICF") # enable COMDAT folding
        flags.append("LTCG:incremental")
        
    #---- embedded IDL ----
    
    #---- windows Metadata ----
    
    #---- advanced ----
    flags.append("DYNAMICBASE") # randomise base address
    flags.append("NXCOMPAT")    # data execution prevention
    flags.append("MACHINE:X64") # target machine
    flags.append("ERRORREPORT:NONE") # Error reporting
    
    return flags

def clang_args(args):
    flags = ["clanj"]
    return flags

def main(argv):
    if len(argv)-1 == len(arg_keys):
        return args_to_flags(argv[1:], msvc_args, clang_args)
    else:
        print("Error: incorrect command line arguments")
        return -1

if __name__ == "__main__":
    main(sys.argv)
