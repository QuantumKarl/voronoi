#if !defined( D_JITTER_SAMPLE_HPP )
#define D_JITTER_SAMPLE_HPP
#include "../geometry/shapes.hpp"

#include <random>
#include <sys_eigen.hpp>

/* A simple ad hoc algorithm which generates points.
	It is generated by:
	the space is divided like a chess board / tiling circles in a grid,
	generate a random number between 0 and 2pi (used as a rotation)
	generate a random number between 0 and radius (used as distance from the center from a cell)
	return all generated points
*/
namespace algorithm::generate
{
using namespace sys;

void jitter_sample( size_t const            i_subdivision,
                    real_t const            i_pass_rate,
                    geometry::box const&    i_area,
                    std::mt19937_64&        io_generator,
                    aligned_vector< vec2 >& io_random_points,
                    aligned_vector< vec2 >& io_ref_points,
                    vec2&                   io_halfwidths );

} // namespace algorithm::generate

#endif
