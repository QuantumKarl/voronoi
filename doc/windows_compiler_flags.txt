
---- general ----
debug information format = fixed
supress startup banner = fixed true
warnings = fixed very high
warnings as errors = variable
diagnostics format = fixed
sdl checks = fixed false
multiprocessor compilaton variable true

---- optimisation ----
optimisation = build variable
inline function expansion = build variable
enable intrinsic functions = build variable
favor size or speed = build variable
omit frame pointers = build variable
enable fiber safe optimisation = build varaible
whole program optimisation = build varaible

---- code generation ----
enable string pooling = fixed false
enable minimal rebuild = build variable
enable C++ exceptions = variable
smaller type check = fixed false
basic runtime checks = build varaible
runtime library = automatic build variable
struct member alignment = variable
security check = fixed false
control flow guard = fixed false
function level linking = fixed false
parralle code generation = variable
enable enhanced instruction set = variable
floating point model = variable
floating point execptions = variable
create hotpatchable image = fixed false
spectre mitigation = fixed false
enable intel jcc erratum mitigation = fixed false

---- language ----
disable language extensions = fixed true
comformance mode = fixed true
treat w:char as built in type = ??
force comformance in for loop scope = ??
remove unereference code and data = ??
enforce type conversion rules = fixed true
enable rtti = variable false
C++ language standard
