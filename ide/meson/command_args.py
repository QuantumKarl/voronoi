# -*- coding: utf-8 -*-

#---------------------------- command args  ----------------------------
# This python script is used in "compiler_flags.py".
# It provides functionality to do with the command line arguments that
# the meson build passes to "compiler_flags.py".

import sys

#---- the command line arguments in order ----
arg_windows = "is_windows" # is windows operating system
arg_linux   = "is_linux"   # is linux operating system
arg_x86     = "is_x86"     # is x86 instruction set architecture
arg_x86_64  = "is_x86_64"  # is x86_64 isntruction set architecture
arg_little  = "is_little"  # is little endian byte order
arg_big     = "is_big"     # is big endian byte order
arg_gcc     = "is_gcc"     # is gcc compiler
arg_clang   = "is_clang"   # is clang compiler
arg_clangcl = "is_clangcl" # is clangcl compiler
arg_msvc    = "is_msvc"    # is msvc compiler
arg_debug   = "has_debug"  # has debug symbols
arg_opt     = "has_opt"    # has optimisations

arg_keys = [arg_windows, arg_linux, # OS
            arg_x86, arg_x86_64, # ISA
            arg_little, arg_big, # endianness
            arg_gcc, arg_clang, arg_clangcl, arg_msvc, # compiler
            arg_debug, arg_opt]  # build type

#--- converts a dictionary to a python object with the same properties as the keys ----
# https://stackoverflow.com/a/1305682/505292
class obj(object):
    def __init__(self, d):
        for a, b in d.items():
            if isinstance(b, (list, tuple)):
               setattr(self, a, [obj(x) if isinstance(x, dict) else x for x in b])
            else:
               setattr(self, a, obj(b) if isinstance(b, dict) else b)

    #---- generic python object to string function ----
    # https://stackoverflow.com/a/49282111/505292
    def __str__(self):
        return  str(self.__class__) + '\n'+ '\n'.join(('{}: {}'.format(item, self.__dict__[item]) for item in self.__dict__))

def args_to_obj(cmd_args):
     # create dictionary of args
    args = {}
    try:
        for i in range(0,len(cmd_args)):
            arg = cmd_args[i]
            assert(arg == "true" or arg == "false")
            args[ arg_keys[i] ] = arg == "true"

        # create python object from dictionary
        return obj(args)
    except:
        print("Error: unable to convert command line arguments into python object\n", sys.exc_info()[0])
        return None

def count_set_keys(args,keys):
    count = 0
    for key in keys:
        val = getattr(args, key)
        count += int(val) # bool to int
    return count

def is_valid_os(args):
    valid = count_set_keys(args,[arg_windows,arg_linux]) == 1
    if not valid:
        print("Error: invalid OS combination")
        print(args)
    return valid

def is_valid_compiler(args):
    valid = count_set_keys(args,[arg_gcc,arg_clang,arg_clangcl,arg_msvc]) == 1
    if not valid:
        print("Error: invalid compiler combination")
        print(args)
        return False
    is_clangcl = args.is_clangcl
    is_msvc    = args.is_msvc
    if is_msvc or is_clangcl:
        if args.is_windows == False:
            print("Error: Windows compiler is set, but Windows is not set as the current OS")
            print(args)
            return False
    is_gcc = args.is_gcc
    if is_gcc:
        print("Error: GCC compiler arguments are not implemented, please use clang instead")
        return False
    return valid

def is_valid_isa(args):
    valid = count_set_keys(args,[arg_x86,arg_x86_64]) == 1
    if not valid:
        print("Error: invalid combination of ISA")
        print(args)
    return valid

def is_valid_endian(args):
    valid = count_set_keys(args,[arg_little, arg_big]) == 1
    if not valid:
        print("Error: invalid endianess")
        print(args)
    return valid

def is_valid_build(args):
    count = count_set_keys(args,[arg_debug,arg_opt])
    if count == 0:
        print("Error: invalid build type, at least debug or optimisations needs to be set")
        print(args)
    return count >= 1 and count <= 2

def args_to_flags(cmd_args, msvc_func, clang_func):
    cmd_args = args_to_obj(cmd_args)
    assert(cmd_args != None)
    assert(is_valid_os(cmd_args))
    assert(is_valid_compiler(cmd_args))
    assert(is_valid_isa(cmd_args))
    assert(is_valid_endian(cmd_args))
    assert(is_valid_build(cmd_args))

    # build flags based on conditions in args object
    flags = []
    if cmd_args.is_windows:
        #NOTE: msvc and clangcl use the same parameters
        assert(cmd_args.is_clangcl or cmd_args.is_msvc)
        flags = msvc_func(cmd_args)
    else:
        assert(cmd_args.is_linux)
        assert(cmd_args.is_gcc==False) # GCC not supported
        flags = clang_func(cmd_args)
    
    # print flags to stdout for meson to consume
    #print each arg for meson to consume via stdout
    if cmd_args.is_windows:
        for flag in flags:
            print("/"+flag)
    else:
        assert(cmd_args.is_linux)
        for flag in flags:
            print("-"+flag)
    return 0
