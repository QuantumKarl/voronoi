
#define ENTRY_CONFIG_IMPLEMENT_MAIN 1
#include "../algorithm/generate/fib_spiral.hpp"
#include "../algorithm/generate/hex_grid.hpp"
#include "../algorithm/generate/jitter_sample.hpp"
#include "../algorithm/generate/reference.hpp"
#include "../algorithm/triangulate/delaunay.hpp"

#include "bgfx_utils.hpp"
#include "common.h"
#include "dialog.hpp"
#include "imgui/imgui.h"

#include <algorithm>
#include <bx/uint32_t.h>
#include <numeric>
#include <random>
#include <sys_eigen.hpp>
#include <vector_display.hpp>

namespace
{
using vec2_t = sys::vec2;
using vec3_t = sys::vec3;

//----------------------------------------------------------------
class voronoi_demo : public entry::AppI
{
  public:
    //--------------------------------
    voronoi_demo( const char* _name, const char* _description )
        : entry::AppI( _name, _description )
        , vector_display_( )
        , gui_( )
        , mouse_state_( )
        , generator_( )
        , random_points_( )
        , random_points_ref_( )
        , width_( )
        , height_( )
        , debug_( )
        , reset_( )
        , prev_width_( )
        , prev_height_( )
    {
    }

    //--------------------------------
    void
        init( int32_t            _argc,
              const char* const* _argv,
              uint32_t           _width,
              uint32_t           _height ) override
    {
        Args args( _argc, _argv );

        width_  = _width;
        height_ = _height;
        debug_  = BGFX_DEBUG_TEXT;
        reset_  = BGFX_RESET_VSYNC;

        bgfx::Init init;
        init.type              = args.m_type;
        init.vendorId          = args.m_pciId;
        init.resolution.width  = width_;
        init.resolution.height = height_;
        init.resolution.reset  = reset_;
        bgfx::init( init );

        const bgfx::RendererType::Enum renderer = bgfx::getRendererType( );
        float texelHalf = bgfx::RendererType::Direct3D9 == renderer ? 0.5f : 0.0f;
        bool  originBottomLeft =
            bgfx::RendererType::OpenGL == renderer || bgfx::RendererType::OpenGLES == renderer;
        vector_display_.init( originBottomLeft, texelHalf );
        vector_display_.setup( uint16_t( width_ ), uint16_t( height_ ) );

        // Enable debug text.
        bgfx::setDebug( debug_ );

        // Set view 0 clear state.
        bgfx::setViewClear( 0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x303030ff, 1.0f, 0 );

        prev_width_  = width_;
        prev_height_ = height_;

        auto generate_points_jitter_func =
            [&]( bool debug_box, int i_subdivision, float i_pass_rate ) {
                if ( debug_box )
                {
                    set_debug_box_bounds( width_, height_ );
                }
                else
                {
                    set_bounds( width_, height_ );
                }
                triangulator_.clear( );
                algorithm::generate::jitter_sample( ( size_t )i_subdivision,
                                                    ( double )i_pass_rate,
                                                    bounds_,
                                                    generator_,
                                                    random_points_,
                                                    random_points_ref_,
                                                    random_points_grid_offset_ );
            };

        auto generate_points_hex_func = [&]( bool debug_box, int i_subdivision ) {
            if ( debug_box )
            {
                set_debug_box_bounds( width_, height_ );
            }
            else
            {
                set_bounds( width_, height_ );
            }
            triangulator_.clear( );
            algorithm::generate::hex_grid( ( size_t )i_subdivision, bounds_, random_points_ );
        };

        auto generate_fib_spiral = [&]( bool debug_box, int i_count, float i_scale ) {
            if ( debug_box )
            {
                set_debug_box_bounds( width_, height_ );
            }
            else
            {
                set_bounds( width_, height_ );
            }
            triangulator_.clear( );
            algorithm::generate::fib_spiral( i_count, i_scale, bounds_, random_points_ );
        };

        auto trianulate_func = [&] {
            if ( random_points_.size( ) >= 2 )
            {
                triangulator_.triangulate( random_points_, bounds_ );
            }
        };

        gui_.init( this,
                   generate_points_jitter_func,
                   generate_points_hex_func,
                   generate_fib_spiral,
                   trianulate_func );

        if ( gui_.get_debug_box( ) )
        {
            set_debug_box_bounds( width_, height_ );
        }
        else
        {
            set_bounds( width_, height_ );
        }

        // draw reference diagram
        algorithm::generate::reference( bounds_, random_points_ );
        triangulator_.triangulate( random_points_, bounds_ );

        imguiCreate( );
    }

    //--------------------------------
    virtual int
        shutdown( ) override
    {
        imguiDestroy( );

        // Cleanup.
        vector_display_.teardown( );

        // Shutdown bgfx.
        bgfx::shutdown( );

        return 0;
    }

    //--------------------------------
    void
        set_debug_box_bounds( int i_width, int i_height, double const offset = 40.0 )
    {
        auto width  = std::min( i_width, 1440 );
        auto height = std::min( i_height, 1440 );

        bounds_.min = vec2_t::Zero( ) + vec2_t( width / 4.0, offset / 2.0 );
        bounds_.max = bounds_.min + vec2_t( height - offset, height - offset );
    }

    //--------------------------------
    void
        set_bounds( int i_width, int i_height )
    {
        auto width  = std::min( i_width, 1440 );
        auto height = std::min( i_height, 1440 );
        bounds_.min = vec2_t::Zero( );
        bounds_.max = vec2_t( width, height );
    }

    //--------------------------------
    bool
        update( ) override
    {
        if ( entry::processEvents( width_, height_, debug_, reset_, &mouse_state_ ) )
        {
            return false;
        }

        imguiBeginFrame(
            mouse_state_.m_mx,
            mouse_state_.m_my,
            ( mouse_state_.m_buttons[entry::MouseButton::Left] ? IMGUI_MBUT_LEFT : 0 ) |
                ( mouse_state_.m_buttons[entry::MouseButton::Right] ? IMGUI_MBUT_RIGHT : 0 ) |
                ( mouse_state_.m_buttons[entry::MouseButton::Middle] ? IMGUI_MBUT_MIDDLE : 0 ),
            mouse_state_.m_mz,
            uint16_t( width_ ),
            uint16_t( height_ ) );

        gui_.draw_dialog( nullptr );

        imguiEndFrame( );

        if ( prev_width_ != width_ || prev_height_ != height_ )
        {
            prev_width_  = width_;
            prev_height_ = height_;
            vector_display_.resize( uint16_t( width_ ), uint16_t( height_ ) );

            if ( gui_.get_debug_box( ) )
            {
                set_debug_box_bounds( width_, height_ );
            }
            else
            {
                set_bounds( width_, height_ );
            }
        }

        const bx::Vec3 at  = { 0.0f, 0.0f, 0.0f };
        const bx::Vec3 eye = { 0.0f, 0.0f, -35.0f };

        float view[16];
        float proj[16];
        bx::mtxLookAt( view, eye, at );
        bx::mtxProj( proj,
                     60.0f,
                     float( width_ ) / float( height_ ),
                     0.1f,
                     100.0f,
                     bgfx::getCaps( )->homogeneousDepth );

        // Set view and projection matrix for view 0.
        bgfx::setViewTransform( 0, view, proj );

        // Set view 0 default viewport.
        bgfx::setViewRect( 0, 0, 0, uint16_t( width_ ), uint16_t( height_ ) );

        // This dummy draw call is here to make sure that view 0 is cleared
        // if no other draw calls are submitted to view 0.
        bgfx::touch( 0 );

        if ( gui_.get_clear_points( ) == true )
        {
            random_points_.clear( );
            random_points_ref_.clear( );
        }
        else
        {
            vector_display_.begin_frame( );
            render_delaunay( );
            render_random_points( );
            render_voronoi( );
            render_bounds( );
            vector_display_.end_frame( );
        }

        // Advance to next frame. Rendering thread will be kicked to
        // process submitted rendering primitives.
        bgfx::frame( );

        return true;
    }

    //--------------------------------
    void
        render_random_points( )
    {
        if ( gui_.get_render_points( ) )
        {
            auto const& colour = gui_.get_points_colour( );
            for ( size_t i = 0, c = random_points_.size( ); i < c; ++i )
            {
                auto const& point     = random_points_[i];
                auto const& ref_point = random_points_ref_[i];

                // draw random point
                vector_display_.set_draw_color( colour[0], colour[1], colour[2] );
                draw_point( point );

                if ( gui_.get_render_ref_points( ) == false || random_points_ref_.size( ) == 0 )
                {
                    continue;
                }
                // else render debug like reference points

                // draw reference square
                vector_display_.set_draw_color( 1.0, 1.0, 1.0 );
                draw_square( ref_point, random_points_grid_offset_ );

                // draw reference line
                vector_display_.set_draw_color( 1.0, 1.0, 0.0 );
                draw_line( ref_point, point );
            }
        }
    }

    //--------------------------------
    void
        render_delaunay( )
    {
        if ( gui_.get_render_delaunay( ) )
        {
            auto const& colour = gui_.get_delaunay_colour( );
            for ( auto& t : triangulator_.triangles( ) )
            {
                vector_display_.set_draw_color( colour[0], colour[1], colour[2] );
                draw_line( t.a( ), t.b( ) );
                draw_line( t.b( ), t.c( ) );
                draw_line( t.c( ), t.a( ) );
                // cycle colours for debug
                //colour = vec3_t( colour.z( ), colour.x( ), colour.y( ) );
            }
        }
        if ( gui_.get_render_circumcircles( ) == true )
        {
            auto const& colour = gui_.get_circumcircle_colour( );
            for ( auto& c : triangulator_.circumcircles( ) )
            {
                vector_display_.set_draw_color( colour[0], colour[1], colour[2] );
                draw_point( c.p );
                draw_circle( c.p, c.r );
            }
        }
    }

    //--------------------------------
    void
        render_voronoi( )
    {
        if ( gui_.get_render_regions( ) )
        {
            auto const& region_colour  = gui_.get_region_colour( );
            auto const& clipped_colour = gui_.get_clipped_region_colour( );
            for ( auto& r : triangulator_.regions( ) )
            {
                auto        poly   = r.create_polygon( );
                auto const& points = poly.get_vec_points( );

                if ( points.empty( ) )
                {
                    continue;
                }

                if ( r.get_clipped( ) )
                {
                    vector_display_.set_draw_color(
                        clipped_colour[0], clipped_colour[1], clipped_colour[2] );
                }
                else
                {
                    vector_display_.set_draw_color(
                        region_colour[0], region_colour[1], region_colour[2] );
                }
                draw_polygon( points );
            }
        }

        if ( gui_.get_render_region_fill( ) )
        {
            auto colour = gui_.get_region_fill_colour( );
            for ( auto& r : triangulator_.regions( ) )
            {
                auto        poly   = r.create_polygon( );
                auto const& points = poly.get_vec_points( );

                if ( points.empty( ) )
                {
                    continue;
                }

                // TODO: cache / pre-calculate
                // TODO: investigate bug where: hex grid of 13 and displaying fill regions goes odd
                vector_display_.set_draw_color( colour[0], colour[1], colour[2] );
                auto const&       aabb   = poly.get_aabb( );
                sys::real_t const length = ( aabb.max - aabb.min ).norm( );
                sys::real_t const step   = 15.0;
                for ( sys::real_t radius = step; radius < length; radius += step )
                {
                    sys::real_t const steps = radius / 1.25;
                    auto       circle       = gen_circle( poly.get_centroid( ), radius, steps );
                    auto       clipped      = poly.clip( circle );
                    auto const new_size     = clipped.get_data_points( ).size( );
                    if ( new_size > 0 )
                    {
                        if ( new_size != points.size( ) &&
                             !algorithm::geometry::nearly_equal( clipped.get_area( ),
                                                                 poly.get_area( ) ) )
                        {
                            draw_polygon( clipped.get_vec_points( ) );
                        }
                        else
                        {
                            draw_polygon( clipped.get_vec_points( ) );
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        if ( gui_.get_render_centroid( ) )
        {
            auto const& centroid_colour = gui_.get_centroid_colour( );
            for ( auto& r : triangulator_.regions( ) )
            {
                auto poly = r.create_polygon( );

                if ( poly.get_data_points( ).empty( ) )
                {
                    continue;
                }

                vector_display_.set_draw_color(
                    centroid_colour[0], centroid_colour[1], centroid_colour[2] );
                draw_point( poly.get_centroid( ) );
            }
        }
    }

    //--------------------------------
    void
        render_bounds( )
    {
        if ( gui_.get_render_bounds( ) == false )
        {
            return;
        }
        vector_display_.set_draw_color( 1.0, 1.0, 1.0 );
        auto const bl = bounds_.min;
        auto const tl = vec2_t( bounds_.min.x( ), bounds_.max.y( ) );
        auto const tr = bounds_.max;
        auto const br = vec2_t( bounds_.max.x( ), bounds_.min.y( ) );
        draw_line( bl, tl );
        draw_line( tl, tr );
        draw_line( tr, br );
        draw_line( bl, br );
    }

    //--------------------------------
    void
        draw_point( vec2_t const& i_point, double const i_size = 6.0 )
    {
        assert( i_size > 0.0 && i_size <= 12.5 );
        auto const half_width = i_size / 2.0;
        vector_display_.draw_box( float( i_point.x( ) - half_width ),
                                  float( i_point.y( ) - half_width ),
                                  float( i_size ),
                                  float( i_size ) );
    }

    //--------------------------------
    void
        draw_line( vec2_t const& i_point_a, vec2_t const& i_point_b )
    {
        vector_display_.draw_line( ( float )i_point_a.x( ),
                                   ( float )i_point_a.y( ),
                                   ( float )i_point_b.x( ),
                                   ( float )i_point_b.y( ) );
    }

    //--------------------------------
    void
        draw_circle( vec2_t const& i_point, double const i_radius )
    {
        double const steps = i_radius < 50.0 ? 32.0 : 64.0f;
        vector_display_.draw_circle(
            float( i_point.x( ) ), float( i_point.y( ) ), float( i_radius ), float( steps ) );
    }

    //--------------------------------
    void
        draw_square( vec2_t const& i_point, vec2_t const& i_half_widths )
    {
        vector_display_.draw_box( static_cast< float >( i_point.x( ) - i_half_widths.x( ) ),
                                  static_cast< float >( i_point.y( ) - i_half_widths.y( ) ),
                                  static_cast< float >( i_half_widths.x( ) * 2.0 ),
                                  static_cast< float >( i_half_widths.y( ) * 2.0 ) );
    }

    //--------------------------------
    void
        draw_polygon( sys::aligned_vector< vec2_t > const& i_points )
    {
        assert( i_points.size( ) > 0 );
        draw_line( i_points.back( ), i_points.front( ) );
        for ( size_t i = 0, c = i_points.size( ) - 1; i < c; ++i )
        {
            draw_line( i_points[i], i_points[i + 1] );
        }
    }

    //--------------------------------
    algorithm::geometry::polygon<>
        gen_circle( vec2_t i_center, sys::real_t _radius, sys::real_t _steps )
    {
        sys::real_t edge_angle = 0.0f;
        sys::real_t ang_adjust = 0.0f;

        sys::real_t const x = i_center.x( );
        sys::real_t const y = i_center.y( );
        sys::real_t step    = double( bx::kPi ) * 2.0 / _steps; // TODO: change kPi is a float
        sys::aligned_vector< vec2_t > points;

        points.emplace_back( x + _radius * std::sin( edge_angle + ang_adjust ),
                             y - _radius * std::cos( edge_angle + ang_adjust ) );
        for ( edge_angle = 0; edge_angle < 2.0 * double( bx::kPi ) - 0.001; edge_angle += step )
        {
            points.emplace_back( x + _radius * std::sin( edge_angle + step - ang_adjust ),
                                 y - _radius * std::cos( edge_angle + step - ang_adjust ) );
        }

        return { std::move( points ) };
    }

    //--------------------------------
    vector_display                   vector_display_;
    dialog                           gui_;
    entry::MouseState                mouse_state_;
    std::mt19937_64                  generator_;
    algorithm::geometry::box         bounds_;
    algorithm::triangluate::delaunay triangulator_;
    sys::aligned_vector< vec2_t >    random_points_;
    sys::aligned_vector< vec2_t >    random_points_ref_;
    vec2_t                           random_points_grid_offset_;
    uint32_t                         width_;
    uint32_t                         height_;
    uint32_t                         debug_;
    uint32_t                         reset_;
    uint32_t                         prev_width_;
    uint32_t                         prev_height_;
};

} // namespace
ENTRY_IMPLEMENT_MAIN( voronoi_demo,
                      "voronoi-demo",
                      "First generate points,\nthen triangulate." );

// vector display example
// (remains here for quick cherry picking)
#if 0
//simplex test
        vector_display_.set_draw_color( 0.7f, 0.7f, 1.0f );
        vector_display_.draw_simplex_font( 50.0f, 80.0f, 1.5f, "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
        vector_display_.draw_simplex_font( 50.0f, 140.0f, 1.5f, "abcdefghijklmnopqrstuvwxyz" );
        vector_display_.set_draw_color( 0.5f, 0.5f, 0.0f );
        vector_display_.draw_simplex_font(
            50.0f, 200.0f, 1.5f, "!@#$%^&*()-=<>/?;:'\"{}[]|\\+=-_" );

        vector_display_.set_draw_color( 1.0f, 0.7f, 0.7f );

        //test pattern for lines
        for ( int ii = 0; ii < 4; ii++ )
        {
            for ( int jj = 0; jj < ii; jj++ ) //draw more intensive lines
            {
                vector_display_.draw_line( 50.0f, 350.0f + 40 * ii, 200.0f, 350.0f + 40 * ii );
            }
        }

        for ( int ii = 0; ii < 4; ii++ )
        {
            for ( int jj = 0; jj <= ii; jj++ )
            {
                vector_display_.draw_line( 50.0f + 40 * ii, 600.0f, 50.0f + 40 * ii, 700.0f );
            }
        }

        //
        // test pattern for shapes
        //
        vector_display_.set_draw_color( 0.7f, 0.7f, 1.0f );
        vector_display_.draw_circle( 250.0f, 450.0f, 10.0f, 32.0f );
        vector_display_.draw_circle( 300.0f, 450.0f, 30.0f, 32.0f );
        vector_display_.draw_circle( 400.0f, 450.0f, 60.0f, 32.0f );
        vector_display_.set_draw_color( 1.0f, 0.0f, 0.0f );
        vector_display_.draw_circle( 500.0f, 450.0f, 80.0f, 64.0f );

        vector_display_.set_draw_color( 0.7f, 1.0f, 0.7f );
        vector_display_.draw_box( 250.0f, 600.0f, 10.0f, 10.0f );
        vector_display_.draw_box( 300.0f, 600.0f, 30.0f, 30.0f );
        vector_display_.draw_box( 350.0f, 600.0f, 60.0f, 60.0f );
        vector_display_.set_draw_color( 0.0f, 1.0f, 0.0f );
        vector_display_.draw_box( 450.0f, 600.0f, 80.0f, 80.0f );

        static float counter = 0.0f;
        counter += 0.01f;

        vector_display_.set_draw_color( 0.0f, 1.0f, 0.0f );
        // draw rotating wheel
        vector_display_.draw_wheel( counter, 800.0f, 450.0f, 80.0f );

        // draw static wheels
        vector_display_.set_draw_color( 1.0f, 0.7f, 1.0f );
        vector_display_.draw_wheel( 3.0f * bx::kPi / 4.0f, 95.0f, 450.0f, 60.0f );
        vector_display_.draw_wheel( bx::kPi / 2.0f, 1150.0f, 450.0f, 30.0f );
        vector_display_.draw_wheel( bx::kPi / 4.0f, 1250.0f, 450.0f, 10.0f );

        const float posX = width_ / 2.0f + bx::sin( counter * 3.18378f ) * ( width_ / 2.0f );
        const float posY = height_ / 2.0f + bx::cos( counter ) * ( height_ / 2.0f );
        vector_display_.draw_circle( posX, posY, 5.0f, 10.0f );

        vector_display_.end_frame( );
#endif
