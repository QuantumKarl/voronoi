#if !defined( D_EIGEN_TYPEDEFS_HPP )
#define D_EIGEN_TYPEDEFS_HPP
#include "sys_platform.hpp"
#include "sys_types.hpp"
#include <cstddef> // for size_t
#include <unordered_set>

DISABLE_WARNINGS

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/StdVector>

ENABLE_WARNINGS

namespace sys
{
// Vectors
template < size_t n >
using vecn = Eigen::Matrix< real_t, n, 1, Eigen::ColMajor >;
using vec2 = vecn< 2 >;
using vec3 = vecn< 3 >;
using vec4 = vecn< 4 >;

// Matrices
template < size_t n >
using matnn = Eigen::Matrix< real_t, n, n, Eigen::ColMajor >;
using mat2  = matnn< 2 >;
using mat3  = matnn< 3 >;
using mat4  = matnn< 4 >;

// Affine transformations
template < size_t n >
using affinen = Eigen::Transform< real_t, n, Eigen::Affine, Eigen::ColMajor >;
using affine2 = affinen< 2 >; // 2D
using affine3 = affinen< 3 >; // 3D
using affine4 = affinen< 4 >; // 4D

template < typename T >
using aligned_vector = std::vector< T, Eigen::aligned_allocator< T > >;

template < typename key,
           typename hasher = std::hash< key >,
           typename keyeq  = std::equal_to< key > >
using aligned_unordered_set =
    std::unordered_set< key, hasher, keyeq, Eigen::aligned_allocator< key > >;

} // namespace sys
#endif
