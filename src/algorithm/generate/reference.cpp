#include "reference.hpp"
#include "../geometry/comparison.hpp"

#include <numeric>

// points / diagram is from: https://upload.wikimedia.org/wikipedia/commons/d/db/Delaunay_circumcircles_vectorial.svg
namespace algorithm::generate
{
void
    reference( geometry::box const& i_area, aligned_vector< vec2 >& io_points )
{
    // original points, relative to (0,0) in a box of (500,500)
    vec2 const                   ref_area( 500, 500 );
    aligned_vector< vec2 > const ref_points = { vec2( 66, 395 ),  vec2( 194, 320 ),
                                                vec2( 243, 394 ), vec2( 373, 345 ),
                                                vec2( 273, 215 ), vec2( 394, 213 ),
                                                vec2( 41, 244 ),  vec2( 143, 40 ),
                                                vec2( 321, 90 ),  vec2( 397, 64 ) };

    real_t const width  = i_area.max.x( ) - i_area.min.x( );
    real_t const height = i_area.max.y( ) - i_area.min.y( );
    assert( width >= 500.0 );
    assert( height >= 500.0 );
    real_t const x_scale = height / ref_area.x( );
    real_t const y_scale = width / ref_area.y( );
    assert( geometry::nearly_equal( x_scale, y_scale ) ); // assumes a square area

    vec2       acc( 0.0, 0.0 );
    vec2 const ref_center =
        std::accumulate( std::begin( ref_points ), std::end( ref_points ), acc ) /
        static_cast< real_t >( ref_points.size( ) );
    vec2 const box_center = ( i_area.min + i_area.max ) / 2.0;
    vec2 const offset     = box_center - ref_center;

    // transpose reference points, scale each point and add them to io_points
    io_points.clear( );
    for ( vec2 const& point : ref_points )
    {
        vec2 const   p   = point + offset;
        vec2 const   pc  = p - box_center;
        vec2 const   dir = pc.normalized( );
        real_t const len = pc.norm( );

        vec2 scaled_point = box_center + ( dir * ( x_scale * len ) );
        io_points.emplace_back( std::move( scaled_point ) );
    }

    auto compare = []( auto const& p1, auto const& p2 ) {
        if ( p1.x( ) < p2.x( ) )
        {
            return true;
        }
        else if ( p1.x( ) > p2.x( ) )
        {
            return false;
        }
        else
        {
            return p1.y( ) < p2.y( );
        }
    };
    std::sort( std::begin( io_points ), std::end( io_points ), compare );
}

} // namespace algorithm::generate
