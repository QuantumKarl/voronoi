#if !defined( D_COLLISION_REGION_HPP )
#define D_COLLISION_REGION_HPP

#include "circumcircle.hpp"
#include "triangle.hpp"

#include <sys_eigen.hpp>

namespace algorithm::triangluate::detail
{
using namespace sys;

//--------------------------------
/* collision_region
    A rectangular region that keeps a list of triangles within it.
    It is updated dynamically during the algorithm.
    It reduces the number of collisions required in the algorithm.
*/
class collision_region
{
  public:
    collision_region( ) noexcept;
    collision_region( real_t const i_min_x, real_t const i_max_x ) noexcept;

    collision_region& operator=( collision_region&& ) = default;
    collision_region& operator=( collision_region const& ) = default;

    bool collide_with_triangle( triangle_itr i_itr ) const;

    void add_triangle( triangle_itr i_itr );
    void remove_triangle( triangle_itr i_itr );

    collision_region& operator+=( collision_region const& i_other );
    void              clear_triangles( );

    triangle_itr_cont_t const& get_triangles( );
    real_t&                    get_min_x( );
    real_t&                    get_max_x( );

  private:
    triangle_itr_cont_t triangles_;
    real_t              min_x_;
    real_t              max_x_;
};

} // namespace algorithm::triangluate::detail

#endif
