#if !defined( D_VALUE_EDGE_HPP )
#define D_VALUE_EDGE_HPP

#include <array>
#include <sys_eigen.hpp>

namespace algorithm::triangluate::detail
{
using namespace sys;

class value_edge final
{
    template < typename T, size_t size >
    union wrapper
    {
        wrapper( )
            : values( )
        {
            values.fill( T( ) );
        }

        std::array< char, sizeof( T ) * size > bytes;
        std::array< T, size >                  values;
    };

  public:
    // NOTE: not pointers, these points are a copy
    // x1, y1, x2, y2
    using points_t = wrapper< real_t, 8 >;

    value_edge( ) noexcept;
    explicit value_edge( vec2 const& i_a ) noexcept;
    value_edge( vec2 const& i_a, vec2 const& i_b ) noexcept;

    void reset( ) noexcept;
    void reset( vec2 const& i_a ) noexcept;
    void reset( vec2 const& i_a, vec2 const& i_b ) noexcept;

    vec2 a( ) const;
    vec2 b( ) const;

    char const* get_ptr( ) const;
    size_t      size( ) const;

    static constexpr size_t
        size_zero( )
    {
        return 0u;
    }

    static constexpr size_t
        size_one( )
    {
        return sizeof( vec2 );
    }

    static constexpr size_t
        size_two( )
    {
        return size_one( ) * 2;
    }

  private:
    points_t points_;
    size_t   size_;
};

} // namespace algorithm::triangluate::detail

#endif
