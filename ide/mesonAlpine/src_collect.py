import os
import sys

fileTypes = [".cpp", ".c"]

def run(path):
	for root, dirs, files in os.walk(path):
		for file in files:
			if( any( map( lambda ext : file.endswith(ext), fileTypes ) ) ):
				print ( os.path.join(root, file) )

def main(argv):
	for i in range(1,len(argv)):
		run( argv[i] )

if __name__ == '__main__':
    main(sys.argv)