#include "value_edge.hpp"

namespace algorithm::triangluate::detail
{
//--------------------------------
value_edge::value_edge( ) noexcept
    : points_( )
    , size_( size_zero( ) )
{
    points_.values.fill( 0.0 );
}

//--------------------------------
value_edge::value_edge( vec2 const& i_a ) noexcept
    : points_( )
    , size_( size_one( ) )
{
    points_.values.fill( 0.0 );
    points_.values[0] = i_a.x( );
    points_.values[1] = i_a.y( );
}

//--------------------------------
value_edge::value_edge( vec2 const& i_a, vec2 const& i_b ) noexcept
    : points_( )
    , size_( size_two( ) )
{
    points_.values.fill( 0.0 );
    points_.values[0] = i_a.x( );
    points_.values[1] = i_a.y( );
    points_.values[2] = i_b.x( );
    points_.values[3] = i_b.y( );
}

//--------------------------------
void
    value_edge::reset( ) noexcept
{
    points_.values.fill( 0.0 );
    size_ = size_zero( );
}
//--------------------------------
void
    value_edge::reset( vec2 const& i_a ) noexcept
{
    points_.values.fill( 0.0 );
    points_.values[0] = i_a.x( );
    points_.values[1] = i_a.y( );
    size_             = size_one( );
}

//--------------------------------
void
    value_edge::reset( vec2 const& i_a, vec2 const& i_b ) noexcept
{
    points_.values.fill( 0.0 );
    points_.values[0] = i_a.x( );
    points_.values[1] = i_a.y( );
    points_.values[2] = i_b.x( );
    points_.values[3] = i_b.y( );
    size_             = size_two( );
}

//--------------------------------
vec2
    value_edge::a( ) const
{
    assert( size( ) == size_one( ) || size( ) == size_two( ) );
    return vec2( points_.values[0], points_.values[1] );
}

//--------------------------------
vec2
    value_edge::b( ) const
{
    assert( size( ) == size_two( ) );
    return vec2( points_.values[2], points_.values[3] );
}

//--------------------------------
char const*
    value_edge::get_ptr( ) const
{
    return static_cast< char const* const >( points_.bytes.data( ) );
}

//--------------------------------
size_t
    value_edge::size( ) const
{
    return size_;
}

} // namespace algorithm::triangluate::detail
