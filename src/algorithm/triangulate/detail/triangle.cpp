#include "triangle.hpp"

#include "circumcircle.hpp"
namespace algorithm::triangluate::detail
{
//--------------------------------
triangle::triangle( vec2 const& i_a,
                    vec2 const& i_b,
                    vec2 const& i_c,
                    circle_itr  i_new_circle ) noexcept
    : points_{ &i_a, &i_b, &i_c }
    , circumcircle_( i_new_circle )
    , bad_( false )
{
}

//--------------------------------
vec2 const&
    triangle::a( ) const
{
    return *points_[0];
}

//--------------------------------
vec2 const&
    triangle::b( ) const
{
    return *points_[1];
}

//--------------------------------
vec2 const&
    triangle::c( ) const
{
    return *points_[2];
}

//--------------------------------
geometry::circle const&
    triangle::circumcircle( ) const
{
    return *circumcircle_;
}

//--------------------------------
void
    triangle::set_bad( )
{
    bad_ = true;
}

//--------------------------------
bool
    triangle::get_bad( ) const
{
    return bad_;
}

//--------------------------------
circle_itr
    triangle::circumcircle_itr( ) const
{
    return circumcircle_;
}

//--------------------------------
bool
    triangle::collide_circumcircle( vec2 const& i_point ) const
{
    return circumcircle_->point_collision( i_point );
}

//--------------------------------
triangle::pointer_edges_t
    triangle::create_pointer_edges( ) const
{
    return { pointer_edge( a( ), b( ) ),
             pointer_edge( b( ), c( ) ),
             pointer_edge( c( ), a( ) ) };
}

//--------------------------------
triangle::value_edges_t
    triangle::create_value_edges( ) const
{
    return { value_edge( a( ), b( ) ), value_edge( b( ), c( ) ), value_edge( c( ), a( ) ) };
}

} // namespace algorithm::triangluate::detail
