#if !defined( D_SYS_GENERIC_HPP )
#define D_SYS_GENERIC_HPP

namespace sys
{
template < class... T >
void
    unused( T&&... )
{
}

} // namespace sys

#endif
