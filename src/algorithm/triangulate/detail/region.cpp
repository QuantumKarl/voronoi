#include "region.hpp"

namespace algorithm::triangluate::detail
{
//--------------------------------
region::region( vec2 const&           i_seed,
                bool                  i_clipped,
                circle_itr_polygon&&  i_poly,
                triangle_itr_cont_t&& i_triangles ) noexcept
    : polygon_( std::forward< circle_itr_polygon >( i_poly ) )
    , triangles_( std::forward< triangle_itr_cont_t >( i_triangles ) )
    , seed_( i_seed )
    , clipped_( i_clipped )
{
}

//--------------------------------
triangle_itr_cont_t const&
    region::get_triangles( ) const
{
    return triangles_;
}

//--------------------------------
vec2 const&
    region::get_seed( ) const
{
    return seed_;
}

//--------------------------------
bool
    region::get_clipped( ) const
{
    return clipped_;
}

//--------------------------------
circle_itr_polygon const&
    region::get_polygon( ) const
{
    return polygon_;
}

//--------------------------------
geometry::polygon<>
    region::create_polygon( ) const
{
    return { polygon_.get_vec_points( ) };
}

} // namespace algorithm::triangluate::detail
