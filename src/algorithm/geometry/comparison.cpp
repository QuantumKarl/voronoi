#include "comparison.hpp"

namespace algorithm::geometry
{
using namespace sys;

bool
    less( vec2 const& center, vec2 const& a, vec2 const& b )
{
    vec2 const ac = a - center;
    vec2 const bc = b - center;
    if ( ac.x( ) >= 0.0 && bc.x( ) < 0.0 )
    {
        return true;
    }
    else if ( ac.x( ) < 0.0 && bc.x( ) >= 0.0 )
    {
        return false;
    }
    else if ( ac.x( ) == 0.0 && bc.x( ) == 0.0 )
    {
        if ( ac.y( ) >= 0.0 || bc.y( ) >= 0.0 )
        {
            return a.y( ) > b.y( );
        }
        else
        {
            return b.y( ) > a.y( );
        }
    }

    // compute the cross product of vectors (center -> a) x (center -> b)
    real_t const det = ( ac.x( ) * bc.y( ) ) - ( bc.x( ) * ac.y( ) );
    if ( det < 0.0 )
    {
        return true;
    }
    else if ( det > 0.0 )
    {
        return false;
    }

    // points a and b are on the same line from the center
    // check which point is closer to the center
    real_t const d1 = ac.norm( );
    real_t const d2 = bc.norm( );

    return d1 > d2;
}

} // namespace algorithm::geometry
