# -*- coding: utf-8 -*-

#---------------------------- compiler flags ----------------------------
# This python script is called from the meson build script.
# It is given bools to indicate the current: compiler, os, isa etc.
# With these bools it then creates command line flags for the compiler.
# Some flags are intended to be toggled. These can be changed via changing
# a variable in the script, see below starting at the "general" comment.

import sys
from enum import Enum, unique
from command_args import args_to_flags, arg_keys

#---- general ----
warnings_as_Errors         = False
multiprocessor_compilation = True
debug_stl                  = False

#---- code generation ----
exceptions = False

@unique
class alignment_opt(Enum):
    # values represent bytes
    default = 0
    one     = 1
    two     = 2
    four    = 4
    eight   = 8
    sixteen = 16
struct_member_alignment = alignment_opt.default

simd_instructions = False

@unique
class floating_point_opt(Enum):
    fast    = "fast"
    precise = "precise"
    strict  = "strict"
floating_point_precision = floating_point_opt.precise

floating_point_exceptions = False

#---- language ----
run_time_type_information = False

@unique
class standard_opt(Enum):
    cpp14 = "c++14"
    cpp17 = "c++17"
standard = standard_opt.cpp17

def msvc_args(args):
    flags = []

    #---- general ----
    flags.append("Zi")     # debug information format
    flags.append("nologo") # supress startup banner

    flags.append("W4")     # all reasonable warnings
    flags.append("w14242") # "identfier": conversion from "type1" to "type1", possible loss of data
    flags.append("w14254") # "operator": conversion from "type1:field_bits" to "type2:field_bits", possible loss of data
    flags.append("w14263") # "function": member function does not override any base class virtual member function
    flags.append("w14265") # "classname": class has virtual functions, but destructor is not virtual instances of this class may not be destructed correctly
    flags.append("w14287") # "operator": unsigned/negative constant mismatch
    flags.append("we4289") # nonstandard extension used: "variable": loop control variable declared in the for-loop is used outside the for-loop scope
    flags.append("w14296") # "operator": expression is always "boolean_value"
    flags.append("w14311") # "variable": pointer truncation from "type1" to "type2"
    flags.append("w14545") # expression before comma evaluates to a function which is missing an argument list
    flags.append("w14546") # function call before comma missing argument list
    flags.append("w14547") # "operator": operator before comma has no effect; expected operator with side-effect
    flags.append("w14549") # "operator": operator before comma has no effect; did you intend "operator"?
    flags.append("w14555") # expression has no effect; expected expression with side-effect
    flags.append("w14619") # pragma warning: there is no warning number "number"
    flags.append("w14640") # Enable warning on thread un-safe static member initialization
    flags.append("w14826") # Conversion from "type1" to "type_2" is sign-extended. This may cause unexpected runtime behavior.
    flags.append("w14905") # wide string literal cast to "LPSTR"
    flags.append("w14906") # string literal cast to "LPWSTR"
    flags.append("w14928") # illegal copy-initialization; more than one user-defined conversion has been implicitly applied

    if warnings_as_Errors: # warnings as errors
        flags.append("WX")
    else:
        flags.append("WX-")

    flags.append("diagnostics:column") # diagnostics format

    if not args.has_opt and args.has_debug:
        flags.append("sdl") # SDL checks

    if multiprocessor_compilation:
        flags.append("MP")

    if args.has_opt:
        if not args.has_debug:
            # flags for all out speed
            flags.append("O2")  # optimisation level
            flags.append("Ob3") # inline function expansion (MSVC 2019 required)
            flags.append("Oi")  # enable intrinsic functions
            flags.append("Ot")  # favor size or speed
            flags.append("Oy")  # omit frame pointers
            # do not enable fiber safe optimisation
            flags.append("GL")  # whole program optimisation
            # TODO: disable assertions
            # TODO: do not debug STL
        else:
            # flags for some speed with debug
            flags.append("Ox")  # optimisation level
            flags.append("Ob0") # disable inline function expansion
            # do not enable intrinsic functions
            # do not favor size or speed
            # do not set omit frame pointers
            # do not enable fiber safe optimisation
            # do not whole program optimisation
            # TODO: enable assertions
            # TODO: debug STL
    else:
        assert(args.has_debug)
        flags.append("Od")  # optimisation level
        flags.append("Ob0") # inline function expansion
        # do not enable intrinsic functions
        # do not favor size or speed
        # do not set omit frame pointers
        # do not enable fiber safe optimisation
        # do not whole program optimisation
        # TODO: enable assertions
        # TODO: debug STL

    #---- code generation ----
    flags.append("GF-") # do not enable string pooling
    flags.append("Gm-") # do not enable minimal rebuild

    if exceptions:
        flags.append("EHsc") # enable C++ exceptions

    # do not set smaller type check

    if args.has_debug:
        flags.append("RTC1") # basic runtime checks

    # runtime library is provided by meson

    if struct_member_alignment != alignment_opt.default:
        flags.append("Zp"+str(struct_member_alignment.value)) # struct member alignment

    if args.has_opt and not args.has_debug:
        flags.append("GS-") # do not security check
    else:
        flags.append("GS") # security check

    # do not set control flow guard

    if args.has_debug:
        flags.append("Gy") # function level linking

    flags.append("Qpar-") # do not parralle code generation (not cross platform)

    if simd_instructions and args.has_opt and not args.has_debug:
        flags.append("arch:AVX2") # enable enhanced instruction set

    flags.append("fp:"+str(floating_point_precision.value)) # floating point model

    if floating_point_exceptions:
        flags.append("fp:except") # enable floating point execptions
    else:
        flags.append("fp:except-") # do not enable floating point execptions

    # do not set create hotpatchable image
    # do not set spectre mitigation
    # do not set enable intel jcc erratum mitigation

    #---- language ----
    # do not disable language extensions, it is required for many windows files
    flags.append("permissive") # do not enable comformance mode
    flags.append("Zc:wchar_t-") # do not treat w:char as built in type
    # do net set force comformance in for loop scope (deprecated)
    flags.append("Zc:inline") # enable remove unereference code and data
    flags.append("Zc:rvalueCast") # enforce type conversion rules

    if run_time_type_information:
        flags.append("GR") # enable rtti
    else:
        flags.append("GR-") # disable rtti

    flags.append("std:"+standard.value) # C++ language standard

    #---- advanced ----
    if args.has_opt and not args.has_debug:
        flags.append("Gr") # calling convention

    flags.append("TP") # compile source as C++
    # do not disable specific warnings
    # do not force include file
    # do not force #using file
    # do not show includes
    flags.append("FC") # use full paths
    # do not omit default library name
    flags.append("errorReport:prompt") # internal compiler error reporting
    # do not treat specific errors as warnings

    return flags

def clang_args(args):
    flags = ["clanj"]
    return flags

def main(argv):
    if len(argv)-1 == len(arg_keys):
        return args_to_flags(argv[1:], msvc_args, clang_args)
    else:
        print("Error: incorrect command line arguments")
        return -1

if __name__ == "__main__":
    main(sys.argv)
